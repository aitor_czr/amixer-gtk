/*
 * globals.h
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

  
#ifndef __GLOBALS_H__
#define __GLOBALS_H__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifdef __cplusplus
#define C_LINKAGE_BEGIN extern "C" {
#define C_LINKAGE_END }
#else
#define C_LINKAGE_BEGIN 
#define C_LINKAGE_END
#endif

#include <stdbool.h>
#include <alsa/asoundlib.h>

#define SOCK_NAME ".amixer_ui.socket"
#define MSG_SIZE 512

typedef struct node {
  int index;	
  const char * name;
  struct node * next;
} node_t;

typedef struct scontrols {	
  node_t * head; 
} scontrols_t;

extern scontrols_t * clist;
extern void * ctx;

struct functions_Ui;
typedef struct functions_Ui functions_Ui;
extern functions_Ui * f_ui;

C_LINKAGE_BEGIN
void sigaction_init(void);
scontrols_t * makelist();
void append(const char *, int, scontrols_t *);
void display(scontrols_t *);
void destroy(scontrols_t *);
int send_msg(const char *);
int alsa_monitor_init(const char *);
int update_values(unsigned int argc, char *argv[], const char *, snd_mixer_selem_channel_id_t, int, long, long, char **);
node_t * createnode(const char *, int);
scontrols_t * makelist();
bool find_control(scontrols_t*, const char*);
void append(const char *, int, scontrols_t *);
void display(scontrols_t *);
void clear(scontrols_t *);
void destroy(scontrols_t *);
int amixer(const char *, unsigned int, const char **);
void * ui_new();
int run_async_snd_monitor(const char *);
void ui_selem_name(void *, const char *);
void ui_index(void *, int);
void ui_volume(void *, bool, bool, bool);
void ui_switch(void *, bool, bool);
void ui_mono(void *, bool);
void ui_limits(void *, long, long);
void ui_raw(void *, long);
void ui_val(void *, long);
void ui_muted(void *, bool);
void ui_delete();
int userinfo_init(void);
const char *username(void);
const char *userhome(void);
C_LINKAGE_END

#endif  // __GLOBALS_H__

 /*
  * functions_ui.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  

#include <iostream>
#include "functions_ui.h"

functions_Ui::functions_Ui()
{
	
}

functions_Ui::~functions_Ui()
{
	
}

functions_Ui::type_signal_ui_delete functions_Ui::signal_ui_delete()
{
   return m_signal_ui_delete;
}

void functions_Ui::do_delete()
{
   m_signal_ui_delete.emit();
}




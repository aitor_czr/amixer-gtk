# paths
PREFIX    ?= /usr

ALSA_DIR := ../alsa
ICON_DIR := ../images

SOUND_ICON := $(ICON_DIR)/sound_24x24.png
NO_SOUND_ICON := $(ICON_DIR)/no_sound_24x24.png
PLAYBACK_ICON := $(ICON_DIR)/playback.png
CAPTURE_ICON := $(ICON_DIR)/capture.png

ALSA_DEPS := $(ALSA_DIR)/amixer.c $(ALSA_DIR)/events.c $(ALSA_DIR)/async.c $(ALSA_DIR)/list.c $(ALSA_DIR)/userinfo.c

OBJECTS := functions_ui.o scontrol.o snd_monitor.o amixer_ui.o statusicon.o popup.o main.o 
SOURCES := $(patsubst %.o,%.cpp,$(OBJECTS))

# compiler and linker
CC := g++

bindir = $(DESTDIR)$(PREFIX)

all: amixer-gtk

.SUFFIXES:

%.o: %.cpp build_images
	$(CC) -Wno-deprecated-declarations -D_GNU_SOURCE -c $< -I.. -I$(ALSA_DIR) `pkg-config --cflags sigc++-2.0 glibmm-2.4 gtkmm-2.4`

.NOTPARALLEL:

amixer-gtk: $(OBJECTS) $(ALSA_DIR)/backend.a
	$(CC) -D_GNU_SOURCE -o $@ $^ -lasound -pthread `pkg-config --libs sigc++-2.0 glibmm-2.4 gtkmm-2.4`

$(ALSA_DIR)/backend.a: $(ALSA_DEPS)
	$(MAKE) -r -C $(ALSA_DIR) backend.a

build_images: $(SOUND_ICON) $(NO_SOUND_ICON) $(PLAYBACK_ICON) $(CAPTURE_ICON)
	gdk-pixbuf-csource --raw --name=sound_24x24 $(SOUND_ICON) >sound_24x24
	gdk-pixbuf-csource --raw --name=no_sound_24x24 $(NO_SOUND_ICON) >no_sound_24x24
	gdk-pixbuf-csource --raw --name=playback_32x37 $(PLAYBACK_ICON) >playback_32x37
	gdk-pixbuf-csource --raw --name=capture_32x37 $(CAPTURE_ICON) >capture_32x37

.SECONDARY: sound_24x24 no_sound_24x24 playback_32x37 capture_32x37

#-----------------------------------------------------

.PHONY: clean cleanall

clean:
	@rm -vf $(wildcard *.o) $(wildcard *~)

cleanall:
	@rm -vf $(wildcard *.o) $(wildcard *~) amixer-gtk sound_24x24 no_sound_24x24 playback_32x37 capture_32x37 


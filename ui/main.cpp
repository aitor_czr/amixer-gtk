/*
 * main.cpp
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <gtkmm/main.h>
#include <iostream>
#include <memory>
#include <vector>
#include <csignal>

#include <unistd.h>
#include <limits.h>
#include <getopt.h>
#include <stdarg.h>

#include <globals.h>
#include "scontrol.h"
#include "amixer_ui.h"
#include "functions_ui.h"

functions_Ui * f_ui;

scontrols_t * clist;
void * ctx;

C_LINKAGE_BEGIN

  void * ui_new()
  {
    return (void *) (new AMixer::SControl);
  }

  void ui_selem_name(void * ctx, const char *name)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_selem_name(name);
    }
  }
  
  void ui_index(void * ctx, int index)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_index(index);
    }
  }
  
  void ui_volume(void * ctx, bool Volume, bool pVolume, bool cVolume)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_volume(Volume, pVolume, cVolume);
    }
  }
  
  void ui_switch(void * ctx, bool sw, bool swj)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_switch(sw, swj);
    }
  }
  
  void ui_mono(void * ctx, bool mono)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_mono(mono);
    }
  }
  
  void ui_limits(void * ctx, long min, long max)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_limits(min, max);
    }
  }
  
  void ui_raw(void * ctx, long value)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_raw(value);
    }
  }
  
  void ui_val(void * ctx, long value)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_val(value);
    }
  }
  
  void ui_muted(void * ctx, bool value)
  {
    if (ctx)
    {
       static_cast<AMixer::SControl *>(ctx)->do_muted(value);
    }
  }

  functions_Ui* new_functions_ui()
  {
    return new functions_Ui();
  }
  
  void ui_delete()
  {
    return f_ui->do_delete();
  }
  
C_LINKAGE_END

static const char *amixer_warning_message = "\n\
Another instance of amixer-gtk \n\
is already running.\n";

static
void signal_handler(int signum)
{ 
   switch(signum) {         
      case SIGTERM:
      case SIGINT:
      case SIGQUIT:
         ui_delete();
         exit(EXIT_SUCCESS);  /* exit gracefully. */
         break;         
      default:
         break;
   }
}

static
bool CheckForAnotherInstance(const char *filename)
{

   FILE *f;
   int pid_fd;
   struct flock fl;
   char *mypid = NULL;
   pid_t pid;
      
   pid_fd = open(filename, O_RDWR|O_CREAT, 0640);
   if(pid_fd < 0) {
      switch(errno) {
            
         case ENOMEM:
            fprintf(stderr, "Cannot open pidfile %s: %s\n", filename, strerror(errno));
            break;
         default:
            fprintf(stderr, "%s: %s\n", filename, strerror(errno) );
            exit(EXIT_FAILURE);
      }
   }
      
   if(asprintf(&mypid, "%ld", (intmax_t) getpid()) == -1)  {  
      fprintf(stderr, "Unable to write pid: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   
   ssize_t sz = 0;
   sz = write(pid_fd, mypid, strlen(mypid));
   if(sz == -1) { 
      fprintf(stderr, "Unable to write pid: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   free(mypid);

   fl.l_type = F_WRLCK; /* F_RDLCK, F_WRLCK, F_UNLCK */
   fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
   fl.l_start = 0; /* Offset from l_whence */
   fl.l_len = 0; /* length, 0 = to EOF */
   fl.l_pid = getpid(); /* our PID */

   // try to create a file lock
   if( fcntl(pid_fd, F_SETLK, &fl) == -1) {  /* F_GETLK, F_SETLK, F_SETLKW */

      // we failed to create a file lock, meaning it's already locked //
      if( errno == EACCES || errno == EAGAIN) {
         return true;
      }
   }

   return false;
}

// print usage statement
static 
void config_usage(const char *progname)
{
   fprintf(stderr, "%s -- Amixer-gtk\n\n"
   "Usage: %s [options]\n\n"
      "Options:\n"
      "   -h --help          this help\n"
      "   -c --card N        select the hw:N card\n"
      "   -D --device        select the device, default 'default'\n"
      "   -s --systray       run in the systray\n"
      "   -x --xpos          x position\n"
      "   -y --ypos          y position\n"
      "   -W --width         window width\n"
      "   -H --height        window height\n"
      "   -i --hide          hide the window\n\n",
      progname, progname);
}

int main(int argc, char *argv[])
{
    unsigned int n, m;
    std::string snd_card;
    snd_pcm_t *pcm_handle = NULL;
    std::vector<AMixer::SControl *> scontents;
    bool occupied_ok = false;
    char lockfile[128]={0};
    bool is_already_running;
    char target[PATH_MAX]={0};

    char *buf = nullptr;
    bool m_systray = false;
    int x_pos = 1500;
    int y_pos = 0;
    int width = 850;
    int height = 480;
    int m_hidden = 0;
    
    std::string filename = "/proc/" + std::to_string(getpid()) + "/exe";
    ssize_t len = ::readlink(filename.c_str(), target, sizeof(target)-1);
    if(len == -1)
       throw std::runtime_error("[ERR]%s: readlink()\n");
    std::string arg0 = std::string(target);    
    std::string progname = std::string(rindex(target, '/') + 1);
  
    if( userinfo_init() )
    {
       printf("Cannot retrieve username and home: %s\n", strerror(errno));
    }
  
    const char *user_name = username();
    const char *user_home = userhome();
   
    strcpy(lockfile, user_home);
    strcat(lockfile, "/.amixer-gtk.lock");
      
    is_already_running = CheckForAnotherInstance(lockfile);
    if(is_already_running) {
       GtkWidget *dialog;
       gtk_init(&argc, &argv);
       dialog = gtk_message_dialog_new(NULL,
          GTK_DIALOG_DESTROY_WITH_PARENT,
          GTK_MESSAGE_WARNING,
          GTK_BUTTONS_OK,
          amixer_warning_message);
       gtk_window_set_title(GTK_WINDOW(dialog), "amixer-gtk warning");
       gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
       gtk_dialog_run(GTK_DIALOG(dialog));
       gtk_widget_destroy(dialog);
       exit(EXIT_FAILURE);
    }
    
    static const struct option long_option[] =
    {
        {"card",    required_argument, 0, 'c'},
        {"device",  required_argument, 0, 'D'},
        {"systray", no_argument,       0, 's'},
        {"xpos",    required_argument, 0, 'x'},
        {"ypos",    required_argument, 0, 'y'},
        {"width",   required_argument, 0, 'W'},
        {"height",  required_argument, 0, 'H'},
        {"hide",    required_argument, 0, 'i'},
        {"help",    no_argument,       0, 'h'},
        {0,         0,                 0,  0 },
    };
        
    buf = (char*)malloc(sizeof(char) * 64);
    if (!buf) {
        fprintf(stderr, "Memory allocation failure\n");
        exit (EXIT_FAILURE);
    }
    
    memset(buf, 0, sizeof(buf));
    
    while (1) {
        int c;
        int option_index = 0;

        if ((c = getopt_long(argc, argv, "c:D:sx:y:W:H:i:h", long_option, &option_index)) < 0)
            break;
            
        switch (c) {
        
        case 's':
            m_systray = true;
            m_hidden = 1;
            break;
        case 'i':
            m_hidden = atoi(optarg);
            break;
        case 'c':
            {
                int i;
                i = snd_card_get_index(optarg);
                if (i >= 0 && i < 32)
                    sprintf(buf, "hw:%i", i);
                else {
                    fprintf(stderr, "Invalid card number.\n");
                    exit(EXIT_FAILURE);
                }
            }
            break;
        case 'D':
            {
                char tmp[64];
                strncpy(tmp, optarg, sizeof(tmp)-1);
                tmp[sizeof(tmp)-1] = '\0';
                sprintf(buf, "%s", tmp);
            }
            break;
        case 'x':
            x_pos = atoi(optarg);
            break;
        case 'y':
            y_pos = atoi(optarg);
            break;
        case 'W':
            width = atoi(optarg);
            break;
        case 'H':
            height = atoi(optarg);
            break;
        case 'h':
            config_usage(progname.c_str());
            free(buf);
            return 0;
        case '?':
            if (isprint (optopt))
               fprintf (stderr, "Unknown option `-%c'.\n", optopt);
            else
               fprintf (stderr, "Unknown option character `\\x%x'.\n",
              optopt);
            free(buf);
            return 1;
        default:
            free(buf);
            abort ();
            
        } // switch        
    } // while
    
    if(*buf != 0) {
        
        snd_card = std::string(buf);
    
    } else {
        int err;
        snd_pcm_t *pcm_handle = NULL;

        int n = 0;
        bool found_ok = false;
        while(!found_ok && n<32) {
    
            int err;
            snd_card = std::string("hw:") + std::to_string(n);
            printf("..%s\n", snd_card.c_str());

            err = snd_pcm_open (&pcm_handle, snd_card.c_str(), SND_PCM_STREAM_PLAYBACK, 0);
            if (err < 0) {
                if(errno == 16) {
                   std::cout << "Device is occupied\n";
                   occupied_ok = true;
                   found_ok = true; // device is occupied
                }
                else
                   n++;
            }    
            else
               found_ok = true;
        }
        
        if(!found_ok) {
    
            std::cout << "Sorry, cannot find sound device\n" << std::endl;
            exit(EXIT_FAILURE);
        
        } else if(!occupied_ok) {
        
            snd_pcm_close(pcm_handle);
        }
    }
        
    std::signal(SIGTERM, signal_handler);
    std::signal(SIGINT, signal_handler);
    std::signal(SIGQUIT, signal_handler);
    
    clist = makelist();

    scontents.begin();
    const char *params[] = { "scontrols", (char*)0 };
    m = sizeof(params)/sizeof(char*) - 1;
    amixer(snd_card.c_str(), m, params);

    node_t * curr = clist->head;
    if(!clist->head) 
        exit(EXIT_FAILURE);
    
    for( ; curr; curr = curr->next) {
        
        ctx = (void *) (new AMixer::SControl);
        std::string ctrl = "'" + std::string(curr->name) + "'," + std::to_string(curr->index);
        const char *params[] = { "sget", ctrl.c_str(), (char*)0 };
        m = sizeof(params)/sizeof(char*) - 1;
        amixer(snd_card.c_str(), m, params);

        auto p = static_cast< AMixer::SControl *>(ctx);
        if(!p->get_volume() && !p->get_pvolume() && !p->get_cvolume()) continue;
        
        scontents.emplace_back(p);

        if( p->get_volume())
            scontents.push_back(std::move(p));
    }
        
    int i = 0;
    for(auto &u : scontents) {
            
        if(u->get_cvolume()) {
                
            std::string name = u->get_selem_name();
            name = name + "," + std::to_string((int)u->get_index());
            u->do_selem_name(name.c_str());
        }    
        
        if(i < scontents.size()-1) {
            
            size_t sz = u->get_selem_name().size();
            if(u->get_volume() && u->get_selem_name() == scontents[i+1]->get_selem_name() && isalpha(u->get_selem_name().at(sz-1))) {
                
                std::string name = u->get_selem_name();
                name = name + "," + std::to_string((int)u->get_index());
                u->do_selem_name(name.c_str());
            }
        }
        
        i++;
    }
    
    std::cout << "\nDevice: " << snd_card << std::endl; 
    
    display(clist);

    f_ui = new functions_Ui();
    
    Gtk::Main kit(argc, argv, true);
    AMixer::AMixer_Ui mixer(snd_card, scontents, arg0, progname, m_systray, x_pos, y_pos, width, height, m_hidden);
    kit.run(mixer);
        
    return EXIT_SUCCESS;
}

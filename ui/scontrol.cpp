/*
 * scontrol.cpp
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include "scontrol.h"

namespace AMixer
{

SControl::SControl()
 :
	mono(false),
	muted(false),
	Volume(false),
	pVolume(false),
	cVolume(false),
	Switch(false),
	SwitchJoined(false)
{
	raw.begin();
	val.begin();
	muted.begin();
	
	signal_selem_name().connect(sigc::mem_fun(*this, &SControl::on_callback_selem_name));
	signal_index().connect(sigc::mem_fun(*this, &SControl::on_callback_index));
	signal_volume().connect(sigc::mem_fun(*this, &SControl::on_callback_volume));
	signal_switch().connect(sigc::mem_fun(*this, &SControl::on_callback_switch));
	signal_mono().connect(sigc::mem_fun(*this, &SControl::on_callback_mono));
	signal_limits().connect(sigc::mem_fun(*this, &SControl::on_callback_limits));
	signal_raw().connect(sigc::mem_fun(*this, &SControl::on_callback_raw));
	signal_val().connect(sigc::mem_fun(*this, &SControl::on_callback_val));
	signal_muted().connect(sigc::mem_fun(*this, &SControl::on_callback_muted));
}

SControl::~SControl()
{
	raw.clear();
	raw.shrink_to_fit();
	
	val.clear();
	val.shrink_to_fit();
	
	muted.clear();
	muted.shrink_to_fit();
}

string SControl::get_selem_name()
{
	return selem_name;
}

int SControl::get_index()
{
	return index;
}

bool SControl::get_volume()
{
	return Volume;
}

bool SControl::get_pvolume()
{
	return pVolume;
}

bool SControl::get_cvolume()
{
	return cVolume;
}

bool SControl::get_switch()
{
	return Switch;
}

bool SControl::get_switch_joined()
{
	return SwitchJoined;
}

bool SControl::get_mono()
{
	return mono;
}

long SControl::get_max()
{
	return max;
}

long SControl::get_min()
{
	return min;
}

vector<long> SControl::get_raw()
{
	return raw;
}

vector<long> SControl::get_val()
{
	return val;
}

vector<bool> SControl::get_muted()
{
	return muted;
}

SControl::type_signal_selem_name SControl::signal_selem_name()
{
   return m_signal_selem_name;
}

void SControl::do_selem_name(const char *name)
{
   m_signal_selem_name.emit(name);
}

void SControl::on_callback_selem_name(const char *name)
{
	selem_name = name;
}

SControl::type_signal_index SControl::signal_index()
{
   return m_signal_index;
}

void SControl::do_index(int idx)
{
   m_signal_index.emit(idx);
}

void SControl::on_callback_index(int idx)
{
	index = idx;
}

SControl::type_signal_volume SControl::signal_volume()
{
   return m_signal_volume;
}

void SControl::do_volume(bool vol, bool pvol, bool cvol)
{
   m_signal_volume.emit(vol, pvol, cvol);
}

void SControl::on_callback_volume(bool vol, bool pvol, bool cvol)
{
	Volume = vol;
	pVolume = pvol;
	cVolume = cvol;
}

SControl::type_signal_switch SControl::signal_switch()
{
   return m_signal_switch;
}

void SControl::do_switch(bool sw, bool swj)
{
   m_signal_switch.emit(sw, swj);
}

void SControl::on_callback_switch(bool sw, bool swj)
{
	Switch = sw;
	SwitchJoined = swj;
}

SControl::type_signal_mono SControl::signal_mono()
{
   return m_signal_mono;
}

void SControl::do_mono(bool value)
{
   m_signal_mono.emit(value);
}

void SControl::on_callback_mono(bool value)
{
	mono = value;
}

SControl::type_signal_limits SControl::signal_limits()
{
   return m_signal_limits;
}

void SControl::do_limits(long n, long m)
{
   m_signal_limits.emit(n, m);
}

void SControl::on_callback_limits(long n, long m)
{
	min = n;
	max = m;
}

SControl::type_signal_raw SControl::signal_raw()
{
   return m_signal_raw;
}

void SControl::do_raw(long value)
{
   m_signal_raw.emit(value);
}

void SControl::on_callback_raw(long value)
{
	raw.push_back(value);
}

SControl::type_signal_val SControl::signal_val()
{
   return m_signal_val;
}

void SControl::do_val(long value)
{
   m_signal_val.emit(value);
}

void SControl::on_callback_val(long value)
{
	val.push_back(value);
}

SControl::type_signal_muted SControl::signal_muted()
{
   return m_signal_muted;
}

void SControl::do_muted(bool value)
{
   m_signal_muted.emit(value);
}

void SControl::on_callback_muted(bool value)
{
	muted.push_back(value);
}

} // namespace AMixer

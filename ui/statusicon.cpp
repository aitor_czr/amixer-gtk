/*
 * statusicon.cpp
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <gtkmm.h>
#include <iostream>

#include "statusicon.h"
#include "amixer_ui.h"
#include "popup.h"

/* PIXBUF DATA */
#include "sound_24x24"
#include "no_sound_24x24"

namespace AMixer
{
	
static
guint32 u8tou32(const guint8* b)
{
  guint32 u;
  int i;
  for(u=*b, i=1; i<4; i++)
    {
      u <<= 8;
      u |= *(b+i);
    }
  return u;
}

/* Destructor (dummy because data is static) */
static
void nop_destroy (guchar *pixels, gpointer data)
{
  /* NO OP */
}

myStatusIcon::myStatusIcon(AMixer_Ui *caller, vector<AMixer::SControl *> scontents)
{
	m_PopupMenu = new myPopup(caller, scontents);
	  
	// StatusIcon's signals (GTK+)
	GtkStatusIcon* gobj_StatusIcon = this->gobj();
	g_signal_connect(G_OBJECT(gobj_StatusIcon), "activate", G_CALLBACK(on_statusicon_activated), caller);
	g_signal_connect(G_OBJECT(gobj_StatusIcon), "popup-menu", G_CALLBACK(on_statusicon_popup), this);
}

myStatusIcon::~myStatusIcon()
{
	if(m_PopupMenu) delete m_PopupMenu;
}

void myStatusIcon::display_menu(guint button, guint32 activate_time)
{
	// Display the popupmenu:
	m_PopupMenu->show_popup_menu(button, activate_time);
}

/* * 
 *
 *  Gtk+
 *
 */ 

void on_statusicon_activated(GtkWidget* widget, gpointer object)
{
	auto parent = static_cast<AMixer_Ui*>(object);
	
	// Send amixer-gtk to the current desktop workspace:
	system("wmctrl -i -r $(wmctrl -l | grep \" amixer-gtk$\" | cut -d ' ' -f 1) -t $(xprop -root -notype _NET_CURRENT_DESKTOP | cut -d ' ' -f 3)");
	
	parent->hidden ? parent->deiconify() : parent->iconify();	
	parent->hidden ? parent->hidden=false : parent->hidden=true;
}

// This wraps the statusicon signal "popup-menu" and calls AMixer_Ui::show_popup_menu()
void on_statusicon_popup(GtkStatusIcon* status_icon,
						 guint button,
						 guint32 activate_time,
						 gpointer object)
{
	return static_cast<myStatusIcon*>(object)->display_menu(button, activate_time);
}

} // namespace AMixer

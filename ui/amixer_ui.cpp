/*
 * amixer_ui.cpp
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include "amixer_ui.h"

#include <iostream>
#include <alsa/mixer.h>
#include <alsa/asoundlib.h>
#include <math.h>
#include <cstring>
#include <ostream>
#include <algorithm>
#include <functional>  // std::ref
#include <filesystem>
#include <memory>
#include <vector>
#include <sstream>      // stringstream
#include <stdexcept>
#include <unistd.h>

#include <globals.h>
#include "scontrol.h"
#include "statusicon.h"
#include "popup.h"
#include "functions_ui.h"

/* PIXBUF DATA */
#include "sound_24x24"
#include "no_sound_24x24"
#include "playback_32x37"
#include "capture_32x37"

namespace AMixer
{

// kill processes stored in /proc/<pid>/task/<pid>/children
// The presence of this file is goberned by CONFIG_PROC_CHILDREN
static
void kill_children()
{
   vector<string> output;
   try {        
      char ch;    
      string str;       
      string strpid = to_string(getpid());
      string fchildproc = "/proc/" + strpid + "/task/" + strpid + "/children";
                
      unique_ptr<FILE, decltype(&fclose)> fp(::fopen(fchildproc.c_str(), "r"), &fclose);            
      while ((ch = (char)fgetc(fp.get()))!=EOF) {            
         if(ch != '\n' && ch != ' ') {              
            stringstream ss;
            ss << ch;
            str.append(ss.str());                
         } else {                
            output.push_back(str);
            str.clear();
         }
      }
   } 
   catch(exception const& e) {            
        
      throw runtime_error("Sorry, there was an error opening '/proc/<pid>/task/<pid>/children'\n"); 
   }
      
   for(auto &u : output)
      kill(atoi(u.c_str()), SIGTERM);
}
    
static
guint32 u8tou32(const guint8* b)
{
    guint32 u;
    int i;
    for(u=*b, i=1; i<4; i++)
    {
        u <<= 8;
        u |= *(b+i);
    }
    return u;
}

/* Destructor (dummy because data is static) */
static
void nop_destroy (guchar *pixels, gpointer data)
{
  /* NO OP */
}

AMixer_Ui::AMixer_Ui(string snd_card, vector<AMixer::SControl *> scontents, string _arg0, string _progname, bool with_systray, int x_pos, int y_pos, int width, int height, int hidden_ok)
:
    m_MonitorThread(nullptr),
    m_HBox(false, 1),
    hidden(true)
{   
   set_border_width(0.2);
   set_default_size(width, height);
   move(x_pos, y_pos);
    
   arg0 = _arg0;
   progname = _progname;
   sndCard = snd_card;
   systray = with_systray;
    
    set_icon(set_sound_icon());
    set_position(Gtk::WIN_POS_CENTER_ON_PARENT);
    set_gravity(Gdk::GRAVITY_NORTH_EAST);
        
    add_events(Gdk::POINTER_MOTION_MASK);
   
    // Create a new scrolled window.
    Gtk::ScrolledWindow *scrolled_window = Gtk::manage(new Gtk::ScrolledWindow());
    scrolled_window->set_border_width(2);
   
    // The policy is one of Gtk::POLICY AUTOMATIC, or Gtk::POLICY_ALWAYS, or Gtk::POLICY_NEVER.
    // Gtk::POLICY_AUTOMATIC will automatically decide whether you need scrollbars, whereas 
    // Gtk::POLICY_ALWAYS will always leave the scrollbars there.
    // The first one is the horizontal scrollbar, the second, the vertical.
    scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    
    add(*scrolled_window);
    scrolled_window->add(m_HBox);
    
    m_Adjustment.begin();
    m_VScale.begin();
    m_Button.begin();
    dB_label.begin();
    
    on_value_changed_connector.begin();
    on_value_toggled_connector.begin();
    
    int i = 0, n = 0;
  
    for(auto &u : scontents) {
        if(!u->get_volume() && !u->get_pvolume() && !u->get_cvolume())
            continue;
    
#if GTK_MAJOR_VERSION == 2

        Gdk::Color *color;
    
#elif GTK_MAJOR_VERSION == 3

        Gdk::RGBA *rgba;
    
#endif
       
        Gtk::VBox *m_VBox = Gtk::manage(new Gtk::VBox());
        m_VBox->set_border_width(5);
        m_HBox.pack_start(*m_VBox, Gtk::PACK_EXPAND_WIDGET, 5);
                
        vector<Gtk::VBox *> vbox;
        vector<Gtk::HBox *> hbox;
        vbox.begin();
        hbox.begin();
        double lower = (double)u->get_min();
        double upper = (double)u->get_max();
        string sndctrl = u->get_selem_name();
        Gtk::Label *m_Label = Gtk::manage(new Gtk::Label( sndctrl.c_str() ));    
        Gtk::Frame *m_Frame = Gtk::manage(new Gtk::Frame( m_Label->get_text() ));
        
#if GTK_MAJOR_VERSION == 3

        //m_Frame->set_label_align(0, 1);
    
#endif
        
        m_VBox->pack_start(*m_Frame, Gtk::PACK_EXPAND_WIDGET, 2);
        
        Gtk::Image *m_Image;
        
        if(u->get_volume()) {
            
                if(scontents[n-1]->get_selem_name() == scontents[n]->get_selem_name()) {
    
#if GTK_MAJOR_VERSION == 2

                    color = new Gdk::Color("#cc491b");
    
#elif GTK_MAJOR_VERSION == 3

                    rgba = new Gdk::RGBA("red");
    
#endif

                    m_Image = Gtk::manage(new Gtk::Image(set_capture_icon()));
                    
                } else {
    
#if GTK_MAJOR_VERSION == 2

                    color = new Gdk::Color("#2754a1");
    
#elif GTK_MAJOR_VERSION == 3

                    rgba = new Gdk::RGBA("blue");
    
#endif
       
                    m_Image = Gtk::manage(new Gtk::Image(set_playback_icon()));
                }
                
        } else if(u->get_pvolume()) {
    
#if GTK_MAJOR_VERSION == 2

            color = new Gdk::Color("#2754a1");
    
#elif GTK_MAJOR_VERSION == 3

            rgba = new Gdk::RGBA("blue");
    
#endif
       
            m_Image = Gtk::manage(new Gtk::Image(set_playback_icon()));
        
        } else if(u->get_cvolume()) {
    
#if GTK_MAJOR_VERSION == 2

            color = new Gdk::Color("#cc491b");
    
#elif GTK_MAJOR_VERSION == 3

            rgba = new Gdk::RGBA("red");
    
#endif

            m_Image = Gtk::manage(new Gtk::Image(set_capture_icon()));
        }
        
        Gtk::VBox *v1 = Gtk::manage(new Gtk::VBox(false, 0));
        m_Frame->add(*v1);
        Gtk::Alignment *m_Align1 = Gtk::manage(new Gtk::Alignment());
        v1->add(*m_Align1);
        
        Gtk::VBox *v2 = Gtk::manage(new Gtk::VBox(false, 0));
        m_Align1->add(*v2);
        v2->pack_start(*m_Image, Gtk::PACK_SHRINK, 5);
        m_Frame->add(*v2);
        Gtk::Alignment *m_Align2 = Gtk::manage(new Gtk::Alignment());
        v2->add(*m_Align2);        
        
        Gtk::HBox *H = Gtk::manage(new Gtk::HBox(true, 2));
        m_Align2->add(*H);
                    
        Gtk::HBox *h = Gtk::manage(new Gtk::HBox(true, 0));
        m_VBox->pack_start(*h, Gtk::PACK_SHRINK, 5);

        int j = 0;
        if( !u->get_mono() ) j = 1;
        int k = i+j+1;
        int l=0;
        bool playback = true;
        bool replicate = false;
        for( ; i<k; i++) {
                            
            /* Use descriptive names for callback's arguments */
            int current = i;
            int previous = i-1;
            
            if( !scontents[n]->get_mono() && i+1 == k )
                playback = false;
            if( scontents[n]->get_volume() && scontents[n]->get_selem_name() == scontents[n-1]->get_selem_name() )
                replicate=true;
            m_VScale.push_back (Gtk::manage(new SndVScale()));
            if(!replicate) {
                long raw_value;
                if(playback) raw_value = scontents[n]->get_raw()[0];
                else raw_value = scontents[n]->get_raw()[1];
    
#if GTK_MAJOR_VERSION == 2

                m_Adjustment.push_back (Gtk::manage(new Gtk::Adjustment(raw_value, lower, upper, 0.1, 0.0, 0.0)));
    
#elif GTK_MAJOR_VERSION == 3

                m_Adjustment.push_back (Gtk::Adjustment::create(raw_value, lower, upper, 0.1, 0.0, 0.0));
    
#endif    
            }
            else {
                // Replicate object
                m_Adjustment.push_back (std::ref(m_Adjustment[i-2]));
            }
    
#if GTK_MAJOR_VERSION == 2

            m_VScale[i]->set_adjustment(*m_Adjustment[i]);
    
#elif GTK_MAJOR_VERSION == 3

            m_VScale[i]->set_adjustment(m_Adjustment[i]);
    
#endif

            m_VScale[i]->name = m_Label->get_text();
            m_VScale[i]->index = scontents[n]->get_index();
            
            if( scontents[n]->get_mono() )
                m_VScale[i]->channel_id = SND_MIXER_SCHN_MONO;
            else if( ( l & 1) == 0 )
                m_VScale[i]->channel_id = SND_MIXER_SCHN_FRONT_LEFT;   /* l is even */
            else            
                m_VScale[i]->channel_id = SND_MIXER_SCHN_FRONT_RIGHT;   /* l is odd */
            
            int idx;
            (m_VScale[i]->channel_id == SND_MIXER_SCHN_FRONT_RIGHT) ? idx = 1 : idx = 0;
                
            m_VScale[i]->set_digits(0);
            m_VScale[i]->set_draw_value();
            m_VScale[i]->set_value_pos(Gtk::POS_TOP);
            m_VScale[i]->set_inverted();
            m_VScale[i]->set_has_tooltip(true);
            m_VScale[i]->set_tooltip_text(m_VScale[i]->name.c_str());
            m_VScale[i]->lower = lower;
            m_VScale[i]->upper = upper;
            m_VScale[i]->SwitchJoined = u->get_switch_joined();
            
            vbox.push_back(Gtk::manage(new Gtk::VBox()));
            H->pack_start(*vbox[l], Gtk::PACK_EXPAND_WIDGET, 0);
            vbox[l]->pack_start(*m_VScale[i], Gtk::PACK_EXPAND_WIDGET, 15);
            
            on_value_changed_connector.push_back
            (
                new sigc::connection
                (
                    m_VScale[current]->signal_value_changed().connect
                    (
                        sigc::bind<int>
                        (
                            sigc::mem_fun(*this->m_VScale[current], &SndVScale::on_value_changed),
                            current
                        )
                    )
                )             
            );
            
            hbox.push_back(Gtk::manage(new Gtk::HBox()));
            vbox[l]->pack_start(*hbox[l], Gtk::PACK_SHRINK, 5);
                
            m_Button.push_back(Gtk::manage(new SndCheckButton()));
            
            on_value_toggled_connector.push_back
            (
                new sigc::connection
                (
                    m_Button[current]->signal_toggled().connect
                    (
                        sigc::bind<int, bool>
                        (
                            sigc::mem_fun(*this->m_Button[current], &SndCheckButton::on_value_toggled),
                            current,
                            u->get_switch_joined()
                        )    
                    )
                )             
            );
            
            on_value_toggled_connector[current]->block();
            
            if(u->get_switch_joined() && (l & 1) != 0 /* odd number */) {
            
                m_Button[current]->signal_toggled().connect
                (
                    sigc::bind<int, int>
                    (
                        sigc::mem_fun(*this->m_Button[previous], &SndCheckButton::on_callback),
                        current,
                        previous
                    )
                );
            
                m_Button[previous]->signal_toggled().connect
                (
                    sigc::bind<int, int>
                    (
                        sigc::mem_fun(*this->m_Button[current], &SndCheckButton::on_callback),
                        previous,
                        current
                    )
                );
            }
           
            if( u->get_switch() ) {
                
                if(m_VScale[i]->muted) m_Button[i]->set_active(false);            
                hbox[l]->pack_start(*m_Button[i], Gtk::PACK_EXPAND_PADDING, 0);
    
#if GTK_MAJOR_VERSION == 3
                
                Gtk::VBox *bb = Gtk::manage(new Gtk::VBox());
                bb->set_size_request(0,23);
                hbox[l]->pack_start(*bb, Gtk::PACK_SHRINK);
        
#endif
            
            } else {
                
                Gtk::VBox *bb = Gtk::manage(new Gtk::VBox());
                bb->set_size_request(0,23);
                hbox[l]->pack_start(*bb, Gtk::PACK_SHRINK);
            
            }
            
            if( u->get_switch() || u->get_switch_joined()) {
                
                m_VScale[i]->muted = scontents[n]->get_muted()[idx];
                if(m_VScale[i]->muted) {
                    m_VScale[i]->set_sensitive(true);
                    m_Button[i]->set_active(true);
                }
                else {
                    m_VScale[i]->set_sensitive(false);
                    m_Button[i]->set_active(false);
                }
            }
            
            dB_label.push_back(Gtk::manage(new Gtk::Label()));
            dB_label[i]->set_angle (-50);
            
#if GTK_MAJOR_VERSION == 2
            
            dB_label[i]->set_size_request(50,60);
            dB_label[i]->modify_fg(Gtk::STATE_NORMAL, *color);
    
#elif GTK_MAJOR_VERSION == 3

            dB_label[i]->set_size_request(50,80);
            dB_label[i]->override_color (*rgba, Gtk::STATE_FLAG_NORMAL);
        
#endif
            
            h->pack_start(*dB_label[i], Gtk::PACK_EXPAND_PADDING, 0);
            
            int dir = 0;
            size_t sz = m_VScale[i]->name.size();
            if( isdigit(m_VScale[i]->name.at(sz-1)) )
                dir = 1;
            
            char *buf = nullptr;            
            char *p[] = { (char*)m_VScale[i]->name.c_str(), (char*)0 };
            update_values( sizeof(p)/sizeof(char*) - 1, p, sndCard.c_str(), SND_MIXER_SCHN_FRONT_RIGHT, dir, (long)m_VScale[i]->lower, (long)m_VScale[i]->upper, &buf );
            
            // We're still completing the list, but we need to block the already existent connectors
            // before adjusting the values of m_Adjustment[i]; otherwise we can end up at a segfault   
            for(auto &u : on_value_changed_connector)
               u->block();
            
            stringstream s_str(buf);     
            string aux;  
            int tok = 0;
            while(getline(s_str, aux, ' ')) {
                if(tok == 0)
                    m_Adjustment[i]->set_value(atof(aux.c_str()));
                else if(tok == 1)
                    dB_label[i]->set_text(aux.c_str());
                tok++;
            }
            free(buf);

            l++;
        }

#if GTK_MAJOR_VERSION == 2

        delete color;
    
#elif GTK_MAJOR_VERSION == 3

        delete rgba;
    
#endif
        
        n++;
    }
   
    dispatcher_connector = new sigc::connection
    (
        m_Dispatcher.connect
        (
            sigc::mem_fun(*this, &AMixer_Ui::get_info_from_snd_monitor)
        )
    );
    
    dispatcher_connector->block();
 
    // Start the worker thread.
    if (!m_MonitorThread)
    {
        m_MonitorThread = new thread (
        [ this ]
            {
                m_Monitor.start(this);
            }
        );
    }
  
    dispatcher_connector->unblock();
    
    delete_event_connector = new sigc::connection
    (
        signal_delete_event().connect
        (
            sigc::mem_fun(*this, &AMixer_Ui::on_delete_event)
        )
    );
    
    f_ui->signal_ui_delete().connect(sigc::mem_fun(*this, &AMixer_Ui::on_callback_ui_delete));
            
    if(systray) {
    
        if(hidden_ok) {
           iconify();
           set_skip_taskbar_hint(true);
           hidden = true;
        }
            
        m_StatusIcon = new myStatusIcon(this, scontents);    
        m_Button[0]->get_active() ? m_StatusIcon->set(set_sound_icon()) : m_StatusIcon->set(set_no_sound_icon());
        
        double x = m_VScale[0]->lower + m_Adjustment[0]->get_value() * 100 / (m_VScale[0]->upper - m_VScale[0]->lower);
        string info = "Master (" + to_string((int)x) + "%)";
        m_StatusIcon->set_tooltip_text(info.c_str());
    }
    
    show_all_children();
    
    run_async_snd_monitor( sndCard.c_str() );
    
    for(auto &u : on_value_changed_connector)
        u->unblock();
    
    for(auto &u : on_value_toggled_connector)
        u->unblock();
}

AMixer_Ui::~AMixer_Ui()
{  
    dB_label.clear();
    dB_label.shrink_to_fit();
    
    m_Adjustment.clear();
    m_Adjustment.shrink_to_fit();
    
    m_VScale.clear();
    m_VScale.shrink_to_fit();
    
    m_Button.clear();
    m_Button.shrink_to_fit();
    
    // Order the worker thread to stop and wait for it to stop.
    if (m_MonitorThread->joinable())
    {
        m_MonitorThread->detach();
        m_Monitor.stop();
        if(m_MonitorThread) {
            delete m_MonitorThread;
            m_MonitorThread = nullptr;
        }
    }
    
    delete dispatcher_connector;
    delete delete_event_connector;
        
    on_value_changed_connector.clear();
    on_value_changed_connector.shrink_to_fit();
        
    on_value_toggled_connector.clear();
    on_value_toggled_connector.shrink_to_fit();
            
    if(systray) delete m_StatusIcon;
    
    destroy(clist);
    
    delete f_ui;
    
    // Terminate childs:  
    kill_children();

    if(restart_ok)
       restart();
}

bool AMixer_Ui::on_delete_event(GdkEventAny*)
{
    if(systray) {
        iconify();
        hidden=true;
    } else {
        hide();
    }
    return true;
}

void AMixer_Ui::notify()
{
    m_Dispatcher.emit();
}

void AMixer_Ui::get_info_from_snd_monitor()
{
    // data to manage
    auto data = make_shared<Data_t>();
    weak_ptr<Data_t> wp;
    
    string str;
    double left_raw_data;
    bool toggled_ok;

    dispatcher_connector->block();

    m_Monitor.get_data(data);
    
    wp = data;
    
    if (auto temp = wp.lock()) {    
       str = temp->name;
       left_raw_data = (double)temp->raw[0];
       toggled_ok = temp->is_toggled;
    } else {
       cout<< "The object is not there.";
       //data.reset();
       dispatcher_connector->unblock();
       for(auto &u : on_value_changed_connector)
          u->unblock();
       return;
    }
   
    int i = 0;
    m_VScale.begin();
    for(auto &u : m_VScale) {
        
        string aux = u->name;
        size_t sz = aux.size();
        if(isalpha( aux.at(sz-1) )) aux = aux + ",0";
        str.erase(std::remove(str.begin(), str.end(), '\''), str.end());
        if(str == aux)
            break;
        else
            i++;
    } 
    
    if(i == m_VScale.size())
        return;
    
    for(auto &u : on_value_changed_connector)
        u->block();
            
    m_VScale[i]->set_value(left_raw_data);
            
    int dir = 0;
    size_t sz = m_VScale[i]->name.size();
    if( isdigit(m_VScale[i]->name.at(sz-1)) )
        dir = 1;
    
    {
        char *buf = nullptr;
        char *p[] = { (char*)m_VScale[i]->name.c_str(), (char*)0 };
        update_values( sizeof(p)/sizeof(char*) - 1, p, sndCard.c_str(), m_VScale[i]->channel_id, dir, (long)m_VScale[i]->lower, (long)m_VScale[i]->upper, &buf );
        stringstream s_str(buf);     
        string aux;  
        int tok = 0;
        while(getline(s_str, aux, ' ')) {
            if(tok == 0)
                m_Adjustment[i]->set_value(atof(aux.c_str()));
            else if(tok == 1)
                dB_label[i]->set_text(aux.c_str());
            tok++;
        }
        free(buf);
    }
    
    if(m_VScale[i+1]->channel_id == SND_MIXER_SCHN_FRONT_RIGHT) { 

       double right_raw_data;
       
       if (auto temp = wp.lock()) {
          right_raw_data = (double)temp->raw[1];
       } else {
          cout<< "The object is not there.";
          //data.reset();
          dispatcher_connector->unblock();
          for(auto &u : on_value_changed_connector)
             u->unblock();
          return;
       }   
         
        m_VScale[i+1]->set_value(right_raw_data);
        
        char *buf = nullptr;
        char *p[] = { (char*)m_VScale[i+1]->name.c_str(), (char*)0 }; 
        update_values( sizeof(p)/sizeof(char*) - 1, p, sndCard.c_str(), SND_MIXER_SCHN_FRONT_RIGHT, dir, (long)m_VScale[i+1]->lower, (long)m_VScale[i+1]->upper, &buf );
        stringstream s_str(buf);     
        string aux;  
        int tok = 0;
        while(getline(s_str, aux, ' ')) {
            if(tok == 0)
                m_Adjustment[i+1]->set_value(atof(aux.c_str()));
            else if(tok == 1)
                dB_label[i+1]->set_text(aux.c_str());
            tok++;
        }
        free(buf);
    }
    
    if(i == 0 && systray) {

       int value;
       
       if (auto temp = wp.lock()) {
          value = (int)temp->val[0];
       } else {
          cout<< "The object is not there.";
          //data.reset();
          dispatcher_connector->unblock();
          for(auto &u : on_value_changed_connector)
             u->unblock();
          return;
       }   
        
       string info = "Master (" + to_string(value) + "%)";
       m_StatusIcon->set_tooltip_text(info.c_str());
    }

    if(toggled_ok) {
       
       int muted_size = 0;
       bool muted_0 = false;
       bool muted_1 = false;
    
       for(auto &u : on_value_toggled_connector)
            u->block();
       
       if (auto temp = wp.lock()) {
          
          muted_0 = temp->muted[0];
          muted_1 = temp->muted[1];
          muted_size = temp->muted.size(); 
          
       } else {
          cout<< "The object is not there.";
          //data.reset();
          dispatcher_connector->unblock();
          for(auto &u : on_value_changed_connector)
             u->unblock();
          return;
       }          
    
       m_Button[i]->set_active(muted_0 ? false : true);
       m_VScale[i]->set_sensitive(muted_0 ? false : true);
          
       if(muted_size > 1 && m_VScale[i+1]->channel_id == SND_MIXER_SCHN_FRONT_RIGHT) {
          m_Button[i+1]->set_active(muted_1 ? false : true);
          m_VScale[i+1]->set_sensitive(muted_1 ? false : true);
       }
          
       if(i == 0 && systray) {
        
          muted_0 ? m_StatusIcon->set(set_no_sound_icon()) : m_StatusIcon->set(set_sound_icon());
          muted_0 ? m_StatusIcon->m_PopupMenu->checkItem->set_active(true) : m_StatusIcon->m_PopupMenu->checkItem->set_active(false);
       }
        
       for(auto &u : on_value_toggled_connector)
          u->unblock();
    }
/*       
    if (auto temp = wp.lock())
       temp.reset(); 
    else
       cout<< "The object is not there.";
 */
    dispatcher_connector->unblock();
    for(auto &u : on_value_changed_connector)
        u->unblock();
}

Glib::RefPtr<Gdk::Pixbuf> AMixer_Ui::set_sound_icon()
{
    Gdk::Pixbuf *pPix;
    const char *magic = "GdkP";
    int i;
    guint32 total_len, pixdata_type, width, height, rowstride;
    const guint8 *pixeldata;
  
    if( strncmp((const char*)sound_24x24, magic, 4) ) {
        fprintf(stderr, "Error in function set_sound_icon(): invalid Gdk pixbuf.\n");
        exit(EXIT_FAILURE);
    }

    total_len = u8tou32(sound_24x24+4);
    pixdata_type = u8tou32(sound_24x24+8);
    rowstride = u8tou32(sound_24x24+12);
    width  = u8tou32(sound_24x24+16);
    height = u8tou32(sound_24x24+20);
    pixeldata = sound_24x24+24;
   
    return Gdk::Pixbuf::create_from_data(pixeldata,
        Gdk::COLORSPACE_RGB, TRUE, 8, width, height, rowstride);
}

Glib::RefPtr<Gdk::Pixbuf> AMixer_Ui::set_no_sound_icon()
{
    Gdk::Pixbuf *pPix;
    const char *magic = "GdkP";
    int i;
    guint32 total_len, pixdata_type, width, height, rowstride;
    const guint8 *pixeldata;
  
    if( strncmp((const char*)no_sound_24x24, magic, 4) ) {
       fprintf(stderr, "Error in function set_sound_icon(): invalid Gdk pixbuf.\n");
       exit(EXIT_FAILURE);
    }

    total_len = u8tou32(no_sound_24x24+4);
    pixdata_type = u8tou32(no_sound_24x24+8);
    rowstride = u8tou32(no_sound_24x24+12);
    width  = u8tou32(no_sound_24x24+16);
    height = u8tou32(no_sound_24x24+20);
    pixeldata = no_sound_24x24+24;
   
    return Gdk::Pixbuf::create_from_data(pixeldata,
       Gdk::COLORSPACE_RGB, TRUE, 8, width, height, rowstride);
}

Glib::RefPtr<Gdk::Pixbuf> AMixer_Ui::set_playback_icon()
{
    Gdk::Pixbuf *pPix;
    const char *magic = "GdkP";
    int i;
    guint32 total_len, pixdata_type, width, height, rowstride;
    const guint8 *pixeldata;
  
    if( strncmp((const char*)playback_32x37, magic, 4) ) {
        fprintf(stderr, "Error in function set_playback_icon(): invalid Gdk pixbuf.\n");
        exit(EXIT_FAILURE);
    }

    total_len = u8tou32(playback_32x37+4);
    pixdata_type = u8tou32(playback_32x37+8);
    rowstride = u8tou32(playback_32x37+12);
    width  = u8tou32(playback_32x37+16);
    height = u8tou32(playback_32x37+20);
    pixeldata = playback_32x37+24;
   
    return Gdk::Pixbuf::create_from_data(pixeldata,
        Gdk::COLORSPACE_RGB, TRUE, 8, width, height, rowstride);
}

Glib::RefPtr<Gdk::Pixbuf> AMixer_Ui::set_capture_icon()
{
    Gdk::Pixbuf *pPix;
    const char *magic = "GdkP";
    int i;
    guint32 total_len, pixdata_type, width, height, rowstride;
    const guint8 *pixeldata;
  
    if( strncmp((const char*)capture_32x37, magic, 4) ) {
        fprintf(stderr, "Error in function set_capture_icon(): invalid Gdk pixbuf.\n");
        exit(EXIT_FAILURE);
    }

    total_len = u8tou32(capture_32x37+4);
    pixdata_type = u8tou32(capture_32x37+8);
    rowstride = u8tou32(capture_32x37+12);
    width  = u8tou32(capture_32x37+16);
    height = u8tou32(capture_32x37+20);
    pixeldata = capture_32x37+24;
   
    return Gdk::Pixbuf::create_from_data(pixeldata,
        Gdk::COLORSPACE_RGB, TRUE, 8, width, height, rowstride);
}

void AMixer_Ui::restart()
{
   int x,y;
   string systray_ok;
   int hidden_ok;
   get_position(x, y);
   restart_ok = false;
   
   systray ? systray_ok = "--systray" : systray_ok = "";   
   hidden ? hidden_ok = 1 : hidden_ok = 0;
	     
   const char * params[] = {
      progname.c_str(),
      systray_ok.c_str(),
      "-D", sndCard.c_str(), 
      "-x", to_string(x).c_str(),
      "-y", to_string(y).c_str(), 
      "-W", to_string(get_width()).c_str(),
      "-H", to_string(get_height()).c_str(), 
      "-i", to_string(hidden_ok).c_str(),
      (char*)0
   };   
      
   execv(arg0.c_str(), (char * const*)params);
}

void AMixer_Ui::on_callback_ui_delete()
{
   AMixer_Ui::~AMixer_Ui();
}

void AMixerSndVScale::on_value_changed(int i)
{
    AMixer_Ui *parent = dynamic_cast<AMixer_Ui*>(this->get_toplevel());
    
    int raw_value = (int)parent->m_Adjustment[i]->get_value();
    
    string chn_id = to_string((int)parent->m_VScale[i]->channel_id);
    const char *params[] = { "sset", (this->name).c_str(), to_string(raw_value).c_str(), chn_id.c_str(), (char*)0 };   
    
    parent->dispatcher_connector->block();
    unsigned int n = sizeof(params)/sizeof(char*) - 1;
    amixer(parent->sndCard.c_str(), n, params);
          
    int dir = 0;
    size_t sz = parent->m_VScale[i]->name.size();
    if( isdigit(parent->m_VScale[i]->name.at(sz-1)) )
        dir = 1;
       
    char *buf = nullptr;
    char *p[] = { (char*)parent->m_VScale[i]->name.c_str(), (char*)0 };
    update_values( sizeof(p)/sizeof(char*) - 1, p, parent->sndCard.c_str(), parent->m_VScale[i]->channel_id, dir, (long)parent->m_VScale[i]->lower, (long)parent->m_VScale[i]->upper, &buf );

    stringstream s_str(buf);     
    string aux;  
    int tok = 0;
    while(getline(s_str, aux, ' ')) {
        if(tok == 0)
            parent->m_Adjustment[i]->set_value(atof(aux.c_str()));
        else if(tok == 1)
            parent->dB_label[i]->set_text(aux.c_str());
        tok++;
    }
    free(buf);
    
    if(i == 0 && parent->systray) {
        
        double x = parent->m_VScale[0]->lower + parent->m_Adjustment[0]->get_value() * 100 / (parent->m_VScale[0]->upper - parent->m_VScale[0]->lower);
        string info = "Master (" + to_string((int)x) + "%)";
        parent->m_StatusIcon->set_tooltip_text(info.c_str());
    }
  
    parent->dispatcher_connector->unblock();
}

void AMixerSndCheckButton::on_value_toggled(int i, bool switch_joined)
{      
     auto parent = dynamic_cast<AMixer_Ui*>(this->get_toplevel());            

/*
    //This would work without the need of any integer as argument:    
    int i = 0;
    for( auto& u : parent->m_Button ) {    
        // Compare memory addresses    
        if( parent->m_Button[i] == this )
            break;
        i++;
    }
    * */
    
    string value;
    bool activated = parent->m_Button[i]->get_active();
    set_active(activated);
    parent->m_VScale[i]->set_sensitive(activated ? true : false);
    
    if(i == 0 && parent->systray) {
        
        Glib::RefPtr<Gdk::Pixbuf> Pix;
        activated ? Pix = parent->set_sound_icon() : Pix = parent->set_no_sound_icon();
        parent->m_StatusIcon->set(Pix);
    }
        
    if(switch_joined && parent->m_VScale[i]->channel_id == SND_MIXER_SCHN_FRONT_RIGHT)
        return;
        
    string chn_id = to_string((int)parent->m_VScale[i]->channel_id);
    const char *params[] = { "sset", parent->m_VScale[i]->name.c_str(), "toggle", chn_id.c_str(), (char*)0 };
    unsigned int n = sizeof(params)/sizeof(char*) - 1;
    
    amixer(parent->sndCard.c_str(), n, params);
}

void AMixerSndCheckButton::on_callback(int i, int j)
{
    auto parent = dynamic_cast<AMixer_Ui*>(this->get_toplevel());      
    bool activated = parent->m_Button[i]->get_active();
    set_active(activated);
    parent->m_VScale[i]->set_sensitive(activated ? true : false);
    parent->m_VScale[j]->set_sensitive(activated ? true : false);
}

} // namespace AMixer

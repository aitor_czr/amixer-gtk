/*
 * scontrol.h
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#ifndef SCONTROL_H
#define SCONTROL_H

#include <iostream>
#include <sigc++/sigc++.h>
#include <vector>

#include <globals.h>

namespace AMixer
{

using namespace std;

class SControl
{
public:
	SControl();
	virtual ~SControl();
	   
	void do_selem_name(const char*);	   
	void do_index(int);
	void do_volume(bool, bool, bool); 
	void do_switch(bool, bool);
	void do_switch_joined(bool, bool);
	void do_mono(bool);
	void do_limits(long, long);
	void do_raw(long);
	void do_val(long);
	void do_muted(bool);
	
	// Accesors
	string get_selem_name();
	int get_index();
	bool get_volume();
	bool get_pvolume();
	bool get_cvolume();
	bool get_switch();
	bool get_switch_joined();
	bool get_mono();
	long get_max();
	long get_min();
	vector<long> get_raw();
	vector<long> get_val();
	vector<bool> get_muted();

	//signal accessors:
	typedef sigc::signal<void(const char*)> type_signal_selem_name;
	typedef sigc::signal<void(int)> type_signal_index;
	typedef sigc::signal<void(bool, bool, bool)> type_signal_volume;
	typedef sigc::signal<void(bool, bool)> type_signal_switch;
	typedef sigc::signal<void(bool)> type_signal_mono;
	typedef sigc::signal<void(long, long)> type_signal_limits;
	typedef sigc::signal<void(long)> type_signal_raw;
	typedef sigc::signal<void(long)> type_signal_val;
	typedef sigc::signal<void(bool)> type_signal_muted;

	type_signal_selem_name signal_selem_name();
	type_signal_index signal_index();
	type_signal_volume signal_volume();
	type_signal_switch signal_switch();
	type_signal_mono signal_mono();
	type_signal_limits signal_limits();
	type_signal_raw signal_raw();
	type_signal_val signal_val();
	type_signal_muted signal_muted();			

protected:
	string selem_name;
	int index;
	bool Volume, pVolume, cVolume;
	bool Switch, SwitchJoined;
	bool mono;
	long max, min;
	vector<long> raw;
	vector<long> val;
	vector<bool> muted;
   
	type_signal_selem_name m_signal_selem_name;
	type_signal_index m_signal_index;   
	type_signal_volume m_signal_volume;   
	type_signal_switch m_signal_switch; 
	type_signal_mono m_signal_mono;        
	type_signal_limits m_signal_limits;  
	type_signal_raw m_signal_raw;  
	type_signal_val m_signal_val;  
	type_signal_muted m_signal_muted;
   
	// Callbacks
	void on_callback_selem_name(const char*);
	void on_callback_index(int);
	void on_callback_volume(bool, bool, bool);
	void on_callback_switch(bool, bool);
	void on_callback_mono(bool);
	void on_callback_limits(long, long);
	void on_callback_raw(long);
	void on_callback_val(long);
	void on_callback_muted(bool);
};

} // namespace AMixer


#endif // SCONTROL_H

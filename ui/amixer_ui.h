/*
 * amixer_ui.h
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#ifndef AMIXER_UI_H
#define AMIXER_UI_H

#include <gtkmm.h>
#include <thread>
#include <memory>
#include <vector>
#include <alsa/asoundlib.h>
#include <sigc++/sigc++.h>

#include <globals.h>

namespace AMixer
{

using namespace std;

// Forward declaration
class SControl;
class myStatusIcon;
class myPopup;

// data to manage
typedef struct Data {
	Data() { 
		mono=true;
		is_toggled=false;
		raw.begin();
		val.begin();
		dB.begin();
		muted.begin();
	}
	Data(string _name, bool _mono, bool _is_toggled, vector<int> _raw, vector<int> _val, vector<double> _dB, vector<bool> _muted) { 
		this->name = _name;
		this->mono = _mono;
		this->is_toggled = _is_toggled;
		this->raw = move(_raw);
		this->val = move(_val);
		this->dB = move(_dB);
		this->muted = move(_muted);
	}
	Data( const Data& ) { }
	Data& operator=(const Data& x) {		
		name = x.name;
		mono = x.mono;		
		is_toggled = x.is_toggled;
		
		for(auto u : x.raw)
			raw.push_back(u);
						
		for(auto u : x.val)
			val.push_back(u);
						
		for(auto u : x.dB)
			dB.push_back(u);
		
		if(x.is_toggled) {				
			for(auto u : x.muted)
				muted.push_back(u);
		}
			
		return *this;
	}
	virtual ~Data() {
		raw.clear();
		raw.shrink_to_fit();
		val.clear();
		val.shrink_to_fit();
		dB.clear();
		dB.shrink_to_fit();
		muted.clear();
		muted.shrink_to_fit();
	}
	void clear() {
		raw.clear();
		val.clear();
		dB.clear();
		muted.clear();
	}
	string name;
	bool mono;
	bool is_toggled;
	vector<int> raw;
	vector<int> val;
	vector<double> dB;
	vector<bool> muted;
} Data_t;

class AMixer_Ui : public Gtk::Window
{
public:
	AMixer_Ui(string, vector<AMixer::SControl *>, string, string, bool, int, int, int, int, int);
	virtual ~AMixer_Ui();
    
    class SndMonitor
    {
        SndMonitor();
        virtual ~SndMonitor();
        
    private:
        shared_ptr<Data_t> mon_data;
        weak_ptr<Data_t> wp_data;
		char *sockpath;
        void start (AMixer_Ui*);         
        void stop ();
        void get_data(shared_ptr<Data_t>&) const;
    
        // Synchronizes access to member data.
        mutable mutex m_Mutex;
        
        bool shall_stop;
        
        // Declare the outer class as friend of the inner class:
        friend class AMixer_Ui;
    };
    
    class SndVScale : public Gtk::VScale
    {
        SndVScale() { };
        virtual ~SndVScale() {};
        
        string name;
        snd_mixer_selem_channel_id_t channel_id;
        bool mono;
        bool muted;
        bool SwitchJoined;
        int index;
		double lower;
		double upper;
        
    private:
		void on_value_changed(int);
        
        // Declare the outer class as friend of the inner class:
        friend class AMixer_Ui;
    };
    
    class SndCheckButton : public Gtk::CheckButton
    {
        SndCheckButton() {};
        virtual ~SndCheckButton() {};
        
    private:
		void on_value_toggled(int, bool);
		void on_callback(int, int);
        
        // Declare the outer class as friend of the inner class:
        friend class AMixer_Ui;
    };

protected:
    string arg0, progname;
    bool restart_ok = false;
	bool hidden, systray;
	string selem_name, sndCard;
	struct sigaction sa;
    
    sigc::connection *dispatcher_connector;
    sigc::connection *delete_event_connector;
    vector<sigc::connection *> on_value_changed_connector;
    vector<sigc::connection *> on_value_toggled_connector;

	//Child widgets:
	myStatusIcon *m_StatusIcon;
	Gtk::HBox m_HBox;
	
	//vector<Glib::RefPtr<Gdk::Pixbuf>> m_Image;
	vector<Gtk::Label *> dB_label;
	
#if GTK_MAJOR_VERSION == 2

	vector<Gtk::Adjustment *> m_Adjustment;
    
#elif GTK_MAJOR_VERSION == 3

	vector<Glib::RefPtr<Gtk::Adjustment>> m_Adjustment;
	
#endif

	vector<SndVScale *> m_VScale;
	vector<SndCheckButton *> m_Button;
		
    Gtk::Window *m_tooltip_window;
    
    SndMonitor m_Monitor;
    thread *m_MonitorThread;
    
    void restart();
    void on_callback_ui_delete();
    
    // Glib:
    Glib::Dispatcher m_Dispatcher;
    
    // Called from the sound monitor:
    void notify();
		
	bool on_delete_event(GdkEventAny* /* event */);
           
    Glib::RefPtr<Gdk::Pixbuf> set_sound_icon();        
    Glib::RefPtr<Gdk::Pixbuf> set_no_sound_icon();
    Glib::RefPtr<Gdk::Pixbuf> set_playback_icon();    
    Glib::RefPtr<Gdk::Pixbuf> set_capture_icon();
    
    // Dispatcher handler:
    void get_info_from_snd_monitor();
	
	// Friend function - Statusicon (GTK+) callback:
	friend void on_statusicon_activated(GtkWidget*, gpointer);
	
	friend class myPopup;
};

typedef AMixer_Ui::SndMonitor AMixerSndMonitor;
typedef AMixer_Ui::SndVScale AMixerSndVScale;
typedef AMixer_Ui::SndCheckButton AMixerSndCheckButton;

} // namespace AMixer


#endif // AMIXER_UI_H

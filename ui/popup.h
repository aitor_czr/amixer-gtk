/*
 * popup.h
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __POPUP_H__
#define __POPUP_H__

#include <gtkmm.h>
#include <vector>

namespace AMixer
{

// Forward declarations:
class AMixer_Ui;
class SControl;

using namespace std;

class myPopup : public Gtk::Window
{    
public:
	myPopup(AMixer_Ui*, vector<AMixer::SControl *>);
	virtual ~myPopup();

protected:
	//Child widgets:
	Gtk::Menu *m_Menu;  
	Gtk::ImageMenuItem *item; 
	Gtk::SeparatorMenuItem *hline;
	Gtk::CheckMenuItem *checkItem;
	
	Gtk::EventBox  *m_EventBox;
	GdkEventButton *gdkEvent;
	
	// Glib:
	Glib::RefPtr<Gtk::UIManager>   m_refUIManager;
	Glib::RefPtr<Gtk::ActionGroup> m_refActionGroup;
	Glib::RefPtr<Gtk::Action>      m_refActionQuit;
	
	// Signal handlers:
	virtual void on_action_mute(AMixer_Ui*);
	virtual void on_action_quit(AMixer_Ui*, vector<AMixer::SControl *>);
	virtual void show_popup_menu(guint, guint32);
	
	// Friend class:
	friend class myStatusIcon;
	friend class AMixer_Ui;
};

} // namespace AMixer

#endif // __POPUP_H__

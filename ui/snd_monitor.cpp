/*
 * snd-monitor.cpp
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <iostream>
#include <thread>
#include <mutex>
#include <memory>
#include <vector>

#include <future>
#include <thread>
#include <chrono>

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <poll.h>
#include <time.h>

#include "amixer_ui.h"


namespace AMixer {
    
using namespace std;

AMixerSndMonitor::SndMonitor()
{    
    shall_stop = false;
}

AMixerSndMonitor::~SndMonitor()
{
    stop();
    ::remove(sockpath);
    free(sockpath);
}

void AMixerSndMonitor::get_data(shared_ptr<Data_t>& data ) const
{
    lock_guard<mutex> lock(m_Mutex);
    if (auto temp = wp_data.lock()) {
	   data = temp;
    } else {
       cout<< "The object is not there.";
       return;
    }
}

void AMixerSndMonitor::stop ()
{
    lock_guard<mutex> lock(m_Mutex);
}

void AMixerSndMonitor::start (AMixer_Ui* caller)
{
	int s1, s2;
	socklen_t t, len;
	struct sockaddr_un local, remote;
	struct pollfd pfd;
	char msg[MSG_SIZE] = {0};
    const char *user_home;
    		
    // The mutex is unlocked here by lock's destructor.
    {
        lock_guard<mutex> lock(m_Mutex);
    }

	if ((s1 = socket(AF_UNIX, SOCK_SEQPACKET | SOCK_CLOEXEC, 0)) == -1) {
		perror("socket");
		exit(1);
	} 
  
	if( userinfo_init() ) {
		
		fprintf(stderr, "Cannot retrieve username and home: %s\n", strerror(errno));
    }
  
	user_home = userhome();
		
	sockpath = (char*)malloc(sizeof(char) * 128);
	if (!sockpath) {
		fprintf(stderr, "Memory allocation failure\n");
		exit (EXIT_FAILURE);
	}
	
	strcpy(sockpath, user_home);
	strcat(sockpath, "/");
	strcat(sockpath, SOCK_NAME);

	local.sun_family = AF_UNIX;
	strcpy(local.sun_path, sockpath);
	unlink(local.sun_path);  Mono:

	len = strlen(local.sun_path) + sizeof(local.sun_family);
	if (bind(s1, (struct sockaddr *)&local, len) == -1) {
		perror("bind");
		exit(1);
	}

	if (listen(s1, 5) == -1) {
		perror("listen");
		exit(1);
	}

	for(;;) {
		
		int n;
		
		t = sizeof(remote);
		if ((s2 = accept(s1, (struct sockaddr *)&remote, &t)) == -1) {
			perror("accept");
			exit(1);
		}
		
		do {
			int pos = 0;
	        string name;
	        bool mono=true;
	        bool is_toggled = false;
	        vector<int> raw;
	        vector<int> val;
	        vector<double> dB;
	        vector<bool> muted;
			vector<string> lines;			
			bool notify_ok = true;
			
			memset(msg, 0, MSG_SIZE);
						
			int rc, m=1;
			pfd.fd = s2;
		    pfd.events = POLLIN;
			rc = poll(&pfd, 1, 0);
			if (!rc) printf("poll timed out\n");
			if (rc < 0) printf("poll failed\n");
			printf("events: 0x%x\n", pfd.revents);
			if (pfd.revents & POLLIN) {
				m = recv(s2, msg, MSG_SIZE, MSG_PEEK);
				printf("There are %d bytes to be read\n", m);
			}

			n = recv(s2, msg, MSG_SIZE, 0);
			if (n <= 0) {
				if (n < 0) perror("recv");
				shall_stop = true;
			}
			
			const char * curr = msg;
			
			// Read message line by line			
			lines.begin();			
			while(curr)
			{
				const char *l = strchr(curr, '\n');
				size_t len = l ? (l-curr) : strlen(curr);
				char * arr = (char*)malloc(sizeof(char) * (len+1));
				if (!arr) {
					fprintf(stderr, "Memory allocation failure\n");
					exit (EXIT_FAILURE);

				}
				memcpy(arr, curr, len);
				arr[len] = '\0';
				lines.push_back(arr);
				free(arr);
				curr = l ? (l+1) : NULL;
			}

			int i = 0;
			for(auto &u : lines) {
				
				if(u.empty())
					break;
				
				if(i == 0) {
					name = u;
					i++;
					continue;
				}
				
				if(i==2)
					mono = false;
						
				string aux;
				vector<string> tokens;
			
				// Vector of string to save tokens
				stringstream ss(u.c_str());
   
				// Tokenizing				
				tokens.begin();
				while(getline(ss, aux, ' ')) {
					tokens.push_back(aux);
				}
		
				raw.push_back(atoi(tokens[0].c_str()));
				val.push_back(atoi(tokens[1].c_str()));
				if(tokens.size()>2) {
					notify_ok = false;
					dB.push_back(atof(tokens[2].c_str()));
				}
				if(tokens.size()>3) {
					is_toggled = true;
					(tokens[3] == "on") ? muted.push_back(false) : muted.push_back(true);
				}

				tokens.clear();
				tokens.shrink_to_fit();
				
				i++;
			}
			
	        if(name.size()>2 && !find_control(clist, name.c_str())) {
		       notify_ok = true;
		       caller->restart_ok = true;  
	           gtk_main_quit();
	        }

			if(!notify_ok) {
				//mon_data = make_shared<Data_t>(name, mono, is_toggled, raw, val, dB, muted);	
				mon_data = make_shared<Data_t>(name, mono, is_toggled, raw, val, dB, muted);
				wp_data = mon_data;					
				caller->notify();
				usleep(5000);
		        raw.clear();
		        raw.shrink_to_fit();
		        val.clear();
		        val.shrink_to_fit();
		        dB.clear();
		        dB.shrink_to_fit();
		        muted.clear();
		        muted.shrink_to_fit();
			}
						
			if (shall_stop)	
				break;
			
		} while (!shall_stop);

		::close(s2);
	}

    {
        lock_guard<mutex> lock(m_Mutex);
        shall_stop = false;
    }
}

} // namespace AMixer

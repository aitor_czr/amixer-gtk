/*
 * statusicon.h
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __STATUSICON_H__
#define __STATUSICON_H__

#include <gtkmm.h>
#include <vector>

namespace AMixer
{

// Forward declarations:
class AMixer_Ui;
class myPopup;
class SControl;

using namespace std;

// Statusicon (GTK+) callbacks:
void on_statusicon_activated(GtkWidget*, gpointer);
void on_statusicon_popup(GtkStatusIcon*, guint, guint32, gpointer);

class myStatusIcon : public Gtk::StatusIcon
{    
public:
	myStatusIcon(AMixer_Ui*, vector<AMixer::SControl *>);
	virtual ~myStatusIcon();

private:
	bool connected;
	
	// Child widgets:
	myPopup *m_PopupMenu;
	Glib::RefPtr<Gtk::StatusIcon> m_refStatusIcon;
	
	// Signal handlers:
	virtual void display_menu(guint, guint32);
	
	// Friend function - Statusicon (GTK+) callback:
	friend void on_statusicon_popup(GtkStatusIcon*, guint, guint32, gpointer);
	
	friend class AMixer_Ui;
};

} // namespace AMixer

#endif // __STATUSICON_H__

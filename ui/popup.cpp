/*
 * popup.cpp
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <gtkmm.h>
#include <iostream>

#include "popup.h"
#include "amixer_ui.h"
#include "scontrol.h"

namespace AMixer
{

myPopup::myPopup(AMixer_Ui *caller, vector<AMixer::SControl *> scontents)
{	
	m_Menu = new Gtk::Menu();
	
	checkItem = Gtk::manage(new Gtk::CheckMenuItem("Mute", true));
	m_Menu->append(*checkItem);
	checkItem->signal_activate().connect (
		sigc::bind<AMixer_Ui*>
		(
			sigc::mem_fun(*this, &myPopup::on_action_mute),
			caller
		)
	);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::PREFERENCES));
	m_Menu->append(*item);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::INFO));
	m_Menu->append(*item);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::HELP));
	m_Menu->append(*item);
	
	hline = Gtk::manage(new Gtk::SeparatorMenuItem());
	m_Menu->append(*hline);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::QUIT));
	m_Menu->append(*item);
	item->signal_activate().connect (
		sigc::bind<AMixer_Ui*, vector<AMixer::SControl *>>
		(
			sigc::mem_fun(*this, &myPopup::on_action_quit),
			caller,
			scontents
		)
	);
	
	caller->m_VScale[0]->get_sensitive() ? checkItem->set_active(false) : checkItem->set_active(true);
	
	m_Menu->show_all_children();
}

myPopup::~myPopup()
{
	if(m_Menu) delete m_Menu;
}

void myPopup::on_action_mute(AMixer_Ui *caller)
{
	if(checkItem->get_active()) {
		caller->m_VScale[0]->set_sensitive(false);
		caller->m_Button[0]->set_active(false);
	} else  {
		caller->m_VScale[0]->set_sensitive(true);
		caller->m_Button[0]->set_active(true);
	}
}

void myPopup::on_action_quit(AMixer_Ui *caller, vector<AMixer::SControl *> scontents)
{	
	bool skip = false;
	int i = 0;
	for(auto &u : scontents) {		
		if(i < scontents.size() - 1)
			scontents[i] == scontents[i+1] ? skip=true : skip=false;
		i++;
		if(skip) {
			skip=false;
			continue;
		}
		delete u;
	}
	
	scontents.clear();
	scontents.shrink_to_fit();
	
    // There is no need to block the connector though ?? and then AMixer_Ui *caller is a superfluous argument
	caller->delete_event_connector->block();

	Gtk::Main::quit();
}

void myPopup::show_popup_menu(guint button, guint32 activate_time)
{ 
	if(m_Menu)
		m_Menu->popup(button, activate_time);
}

} // namespace AMixer

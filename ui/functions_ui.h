 /*
  * functions_ui.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  

#ifndef __FUNCTIONS_UI_H__
#define __FUNCTIONS_UI_H__

#include <sigc++/sigc++.h>
#include <globals.h>

class functions_Ui {
public:
   functions_Ui();
   virtual ~functions_Ui();
   
   void do_delete();	

   //signal accessors:
   typedef sigc::signal<void()> type_signal_ui_delete;
   
   type_signal_ui_delete  signal_ui_delete();

protected:
   type_signal_ui_delete  m_signal_ui_delete;
};

#endif  // __FUNCTIONS_UI_H__

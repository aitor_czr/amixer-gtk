
GUI_DIR   := ui
ALSA_DIR := alsa

DESTDIR ?=
PREFIX  ?= /usr/local
BINDIR  ?= $(DESTDIR)$(PREFIX)/bin

VPATH := $(GUI_DIR):$(ALSA_DIR)

INSTALLED_FILES := \
	$(BINDIR)/amixer-gtk \
	$(DESTDIR)$(PREFIX)/share/applications/amixer-gtk.desktop \
	$(DESTDIR)/usr/share/pixmaps/amixer-gtk.png

.SUFFIXES:

all: $(GUI_DIR)/amixer-gtk

#--------- Executable
$(GUI_DIR)/amixer-gtk: $(GUI_DEPS)
	@$(MAKE) -r -C $(GUI_DIR) amixer-gtk

$(WATCH_DIR)/alsa.a: $(WATCH_DEPS)
	@$(MAKE) -r -C $(WATCH_DIR) alsa.a

#--------- Installation
# Essentials:
install: $(INSTALLED_FILES)

$(BINDIR)/amixer-gtk: $(GUI_DIR)/amixer-gtk
	cp $< $@

$(DESTDIR)$(PREFIX)/share/applications/amixer-gtk.desktop: amixer-gtk.desktop
	mkdir -p $(DESTDIR)$(PREFIX)/share/applications
	cp $< $@

$(DESTDIR)/usr/share/pixmaps/amixer-gtk.png: images/amixer-gtk.png
	mkdir -p $(DESTDIR)/usr/share/pixmaps
	cp $< $@

#------- Cleaning
clean:
	$(MAKE) -r -C $(ALSA_DIR) clean
	$(MAKE) -r -C $(GUI_DIR) clean

cleanall:
	$(MAKE) -r -C $(ALSA_DIR) cleanall
	$(MAKE) -r -C $(GUI_DIR) cleanall

uninstall:
	rm -vf $(INSTALLED_FILES)

.PHONY: clean cleanall uninstall
.SILENT: clean cleanall uninstall

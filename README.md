# amixer-gtk

Desktop gtk mixer application for the Linux sound sytem ALSA with features similar 
to alsamixer.

<p align="center">
  <img src="images/screenshot.png">
</p>


## Installation via APT

Use the [APT](https://wiki.debian.org/Apt) package manager to install amixer-gtk.

```bash
apt-get install amixer-gtk
```


## Building from source

If you are using a different package management system, then you'll need to build 
the project from source.

### Build dependencies

- `g++`
- `make`
- `gtkmm-2.4 or gtkmm-3.0`  (devel)
- `libasound2`              (devel)
- `gettext`    - required in the near future
- `intltool`   - required in the near future

### Runtime dependency

- `wmctrl`

### Procedure

Download the tarball from the devuan git repository [amixer-gtk](https://git.devuan.org/aitor_czr/amixer-gtk/archive/master.tar.gz), 
extract it with `tar xvf master.tar.gz` and go into the extracted folder. There, run the 
following commands: 

```sh
$ make
$ sudo make install
```


## Copying

Amixer-gtk is distributed under the terms in the COPYING text file that
comes with this package.


Authors
-------

Amixer-gtk was written by:

Aitor C.Z. <aitor_czr@gnuinos.org>

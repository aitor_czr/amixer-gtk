/* 
  userinfo.c -- Copyright (C) 2019 Didier Kryn <kryn@in2p3.fr>
  
  Permission to  use, copy, modify, distribute, and sell this software and its
  documentation for any purpose  is hereby granted without fee,  provided that
  the above copyright notice appear in all copies and that both that copyright
  notice and this permission notice appear in supporting documentation.
  
  The above copyright notice  and this permission notice  shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED  "AS IS",  WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING  BUT NOT LIMITED  TO  THE WARRANTIES OF  MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE  AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES  OR OTHER  LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
  IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  
  Except as contained  in this notice, the name of copyright holders shall not
  be used  in  advertising  or  otherwise  to promote  the sale,  use or other
  dealings  in this  Software  without prior  written  authorization  from the
  copyright holders.
  
  ----------------------------------------------------------------------------

  Go to the git repository for more info:

  https://git.devuan.org/kryn/hopman.git 

  All modifications to the original source file are:
  Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>

*/

 
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>
#include <errno.h>
#include <string.h>

static char *configbuf;
static size_t configlen;

/* offsets of username and home in configbuf: */
int name_offset, home_offset;
/* We save offsets rather than pointers because configbuf may be realloc()ed */

/*------------------------- obtain username and home ------------------------*/
int userinfo_init(void)
{
  char pwbuf[1024];
  struct passwd pw, *ppw;
  uid_t uid;
  size_t len, lenname, lenhome;
  char *newbuf;

  uid = getuid();
  errno = getpwuid_r(uid, &pw, pwbuf, 1024, &ppw);
  if( errno )
    {
      name_offset = home_offset = -1;
      return -1;
    }
  else if( !ppw)
    {
      name_offset = home_offset = -1;
      return 0;
    }

  lenname = strlen(ppw->pw_name);
  lenhome = strlen(ppw->pw_dir);

  /* We do not assume that configbuf is empty. At startup, configbuf is NULL
     and configlen is 0, but they may have been changed before userinfo_init 
     is invoked. */
  
  len = configlen + lenname + lenhome + 2;
  newbuf = realloc(configbuf, len);
  if(!newbuf)
    {
      name_offset = home_offset = -1;
      return -1;
    }

  configbuf = newbuf;

  /* store username and userhome at the end of configuration buffer */
  strcpy(configbuf+configlen, ppw->pw_name);
  strcpy(configbuf+configlen+lenname+1, ppw->pw_dir);
  name_offset = configlen;
  home_offset = configlen+lenname+1;
  configlen = len;
  return 0;
}

const char *username(void)
{
  if(name_offset == -1) return NULL;
  else return configbuf + name_offset;
}

const char *userhome(void)
{
  if(home_offset == -1) return NULL;
  else return configbuf + home_offset;
}


/*
  events.c
  This file is part of amixer.c - ALSA command line mixer utility
  Copyright (c) 1999-2000 by Jaroslav Kysela <perex@perex.cz>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  All modifications to the original source file are:
  Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>

 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <alsa/asoundlib.h>
#include <alsa/control.h>
#include <getopt.h>
#include <math.h>

#include <globals.h>

#define LEVEL_BASIC		(1<<0)
#define LEVEL_INACTIVE	(1<<1)
#define LEVEL_ID		(1<<2)

static snd_mixer_t *handle;
static int smixer_level = 0;
static struct snd_mixer_selem_regopt smixer_options;
static char card[64];

struct volume_ops {
	int (*get_range)(snd_mixer_elem_t *elem, long *min, long *max);
	int (*get)(snd_mixer_elem_t *elem, snd_mixer_selem_channel_id_t c,
		   long *value);
	int (*set)(snd_mixer_elem_t *elem, snd_mixer_selem_channel_id_t c,
		   long value, int dir);
};

/* FIXME: normalize to int32 space to be compatible with other types */
#define MAP_VOL_RES	(INT32_MAX / 100)
	
enum { VOL_RAW, VOL_DB, VOL_MAP };

struct volume_ops_set {
	int (*has_volume)(snd_mixer_elem_t *elem);
	struct volume_ops v[3];
};

static int set_playback_dB(snd_mixer_elem_t *elem,
			   snd_mixer_selem_channel_id_t c, long value, int dir)
{
	return snd_mixer_selem_set_playback_dB(elem, c, value, dir);
}

static int set_capture_dB(snd_mixer_elem_t *elem,
			  snd_mixer_selem_channel_id_t c, long value, int dir)
{
	return snd_mixer_selem_set_capture_dB(elem, c, value, dir);
}

static int set_playback_raw_volume(snd_mixer_elem_t *elem,
				   snd_mixer_selem_channel_id_t c,
				   long value, int dir)
{
	return snd_mixer_selem_set_playback_volume(elem, c, value);
}

static int set_capture_raw_volume(snd_mixer_elem_t *elem,
				  snd_mixer_selem_channel_id_t c,
				  long value, int dir)
{
	return snd_mixer_selem_set_capture_volume(elem, c, value);
}

static const struct volume_ops_set vol_ops[2] = {
	{
		.has_volume = snd_mixer_selem_has_playback_volume,
		.v = {{ snd_mixer_selem_get_playback_volume_range,
			snd_mixer_selem_get_playback_volume,
			set_playback_raw_volume },
		      { snd_mixer_selem_get_playback_dB_range,
			snd_mixer_selem_get_playback_dB,
			set_playback_dB },
		},
	},
	{
		.has_volume = snd_mixer_selem_has_capture_volume,
		.v = {{ snd_mixer_selem_get_capture_volume_range,
			snd_mixer_selem_get_capture_volume,
			set_capture_raw_volume },
		      { snd_mixer_selem_get_capture_dB_range,
			snd_mixer_selem_get_capture_dB,
			set_capture_dB },
		},
	},
};

static int std_vol_type = VOL_RAW;

static void error(const char *fmt,...)
{
	va_list va;

	va_start(va, fmt);
	fprintf(stderr, "amixer: ");
	vfprintf(stderr, fmt, va);
	fprintf(stderr, "\n");
	va_end(va);
}

/* Fuction to convert from volume to percentage. val = volume */
static int convert_prange(long val, long min, long max)
{
	long range = max - min;
	int tmp;

	if (range == 0)
		return min;
	val -= min;
	tmp = rint((double)val/(double)range * 100);
	return tmp;
}

static void print_dB(long dB, char **buf)
{	
	*buf = (char*)malloc(sizeof(char) * 16);
	if (!*buf) {
		fprintf(stderr, "Memory allocation failure\n");
		exit (EXIT_FAILURE);
	}  
	
	if (dB < 0) {
		sprintf(*buf, " -%li.%02li", -dB / 100, -dB % 100);
	} else {
		sprintf(*buf, " %li.%02li", dB / 100, dB % 100);
	}
}

static void show_selem_volume(snd_mixer_elem_t *elem, 
			      snd_mixer_selem_channel_id_t chn, int dir,
			      long min, long max, char **buf)
{
	long raw, val;
		
	*buf = (char*)malloc(sizeof(char) * MSG_SIZE);
	if (!*buf) {
		fprintf(stderr, "Memory allocation failure\n");
		exit (EXIT_FAILURE);
	} 
	
	vol_ops[dir].v[VOL_RAW].get(elem, chn, &raw);
	if (std_vol_type == VOL_RAW)
		val = convert_prange(raw, min, max);
	else {
		vol_ops[dir].v[std_vol_type].get(elem, chn, &val);
		val = convert_prange(val, 0, MAP_VOL_RES);
	}
	sprintf(*buf, "%li %li%%", raw, val);
	if (!vol_ops[dir].v[VOL_DB].get(elem, chn, &val)) {
		char *str = NULL;
		print_dB(val, &str);
		strcat(*buf, str);
		free(str);
	}
}

static int show_selem(snd_mixer_selem_id_t *id, const char *space, int level, char **buf)
{
	snd_mixer_selem_channel_id_t chn;
	long pmin = 0, pmax = 0;
	long cmin = 0, cmax = 0;
	int psw, csw;
	int pmono, cmono, mono_ok = 0;
	snd_mixer_elem_t *elem;
	
	elem = snd_mixer_find_selem(handle, id);
	if (!elem) {
		error("Mixer %s simple element not found", card);
		return -ENOENT;
	}

	if (level & LEVEL_BASIC) {
		if (snd_mixer_selem_has_playback_volume(elem) ||
		    snd_mixer_selem_has_playback_switch(elem)) {
			printf("%sPlayback channels:", space);
			if (snd_mixer_selem_is_playback_mono(elem)) {
				printf(" Mono");
			} else {
				int first = 1;
				for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++){
					if (!snd_mixer_selem_has_playback_channel(elem, chn))
						continue;
					if (!first)
						printf(" -");
					printf(" %s", snd_mixer_selem_channel_name(chn));
					first = 0;
				}
			}
			printf("\n");
		}
		if (snd_mixer_selem_has_capture_volume(elem) ||
		    snd_mixer_selem_has_capture_switch(elem)) {
			printf("%sCapture channels:", space);
			if (snd_mixer_selem_is_capture_mono(elem)) {
				printf(" Mono");
			} else {
				int first = 1;
				for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++){
					if (!snd_mixer_selem_has_capture_channel(elem, chn))
						continue;
					if (!first)
						printf(" -");
					printf(" %s", snd_mixer_selem_channel_name(chn));
					first = 0;
				}
			}
			printf("\n");
		}
		if (snd_mixer_selem_has_playback_volume(elem) ||
		    snd_mixer_selem_has_capture_volume(elem)) {
			printf("%sLimits:", space);
			if (snd_mixer_selem_has_common_volume(elem)) {
				snd_mixer_selem_get_playback_volume_range(elem, &pmin, &pmax);
				snd_mixer_selem_get_capture_volume_range(elem, &cmin, &cmax);
				printf(" %li - %li", pmin, pmax);
			} else {
				if (snd_mixer_selem_has_playback_volume(elem)) {
					snd_mixer_selem_get_playback_volume_range(elem, &pmin, &pmax);
					printf(" Playback %li - %li", pmin, pmax);
				}
				if (snd_mixer_selem_has_capture_volume(elem)) {
					snd_mixer_selem_get_capture_volume_range(elem, &cmin, &cmax);
					printf(" Capture %li - %li", cmin, cmax);
				}
			}
			printf("\n");
		}
		pmono = snd_mixer_selem_has_playback_channel(elem, SND_MIXER_SCHN_MONO) &&
		        (snd_mixer_selem_is_playback_mono(elem) || 
			 (!snd_mixer_selem_has_playback_volume(elem) &&
			  !snd_mixer_selem_has_playback_switch(elem)));
		cmono = snd_mixer_selem_has_capture_channel(elem, SND_MIXER_SCHN_MONO) &&
		        (snd_mixer_selem_is_capture_mono(elem) || 
			 (!snd_mixer_selem_has_capture_volume(elem) &&
			  !snd_mixer_selem_has_capture_switch(elem)));
#if 0
		printf("pmono = %i, cmono = %i (%i, %i, %i, %i)\n", pmono, cmono,
				snd_mixer_selem_has_capture_channel(elem, SND_MIXER_SCHN_MONO),
				snd_mixer_selem_is_capture_mono(elem),
				snd_mixer_selem_has_capture_volume(elem),
				snd_mixer_selem_has_capture_switch(elem));
#endif
		if (pmono || cmono) {
			if (!mono_ok) {
				printf("%s%s:", space, "Mono");
				mono_ok = 1;
			}
			if (snd_mixer_selem_has_common_volume(elem)) {
				char *str = NULL;
				show_selem_volume(elem, SND_MIXER_SCHN_MONO, 0, pmin, pmax, &str);
				strcat(*buf, str);
				free(str);
			}
			if (snd_mixer_selem_has_common_switch(elem)) {
				snd_mixer_selem_get_playback_switch(elem, SND_MIXER_SCHN_MONO, &psw);
				printf(" [%s]", psw ? "on" : "off");
				psw ? strcat(*buf, " on") : strcat(*buf, " off");
			}
		}
		if (pmono && snd_mixer_selem_has_playback_channel(elem, SND_MIXER_SCHN_MONO)) {
			int title = 0;
			if (!mono_ok) {
				printf("%s%s:", space, "Mono");
				mono_ok = 1;
			}
			if (!snd_mixer_selem_has_common_volume(elem)) {
				if (snd_mixer_selem_has_playback_volume(elem)) {
					char *str = NULL;
					printf(" Playback");
					title = 1;
					show_selem_volume(elem, SND_MIXER_SCHN_MONO, 0, pmin, pmax, &str);
					strcat(*buf, str);
					free(str);
				}
			}
			if (!snd_mixer_selem_has_common_switch(elem)) {
				if (snd_mixer_selem_has_playback_switch(elem)) {
					if (!title)
						printf(" Playback");
					snd_mixer_selem_get_playback_switch(elem, SND_MIXER_SCHN_MONO, &psw);
					printf(" [%s]", psw ? "on" : "off");
					psw ? strcat(*buf, " on") : strcat(*buf, " off");
				}
			}
		}
		if (cmono && snd_mixer_selem_has_capture_channel(elem, SND_MIXER_SCHN_MONO)) {
			int title = 0;
			if (!mono_ok) {
				printf("%s%s:", space, "Mono");
				mono_ok = 1;
			}
			if (!snd_mixer_selem_has_common_volume(elem)) {
				if (snd_mixer_selem_has_capture_volume(elem)) {
					char *str = NULL;
					printf(" Capture");
					title = 1;
					show_selem_volume(elem, SND_MIXER_SCHN_MONO, 1, cmin, cmax, &str);
					strcat(*buf, str);
					free(str);
				}
			}
			if (!snd_mixer_selem_has_common_switch(elem)) {
				if (snd_mixer_selem_has_capture_switch(elem)) {
					if (!title)
						printf(" Capture");
					snd_mixer_selem_get_capture_switch(elem, SND_MIXER_SCHN_MONO, &csw);
					printf(" [%s]", csw ? "on" : "off");
					csw ? strcat(*buf, " on") : strcat(*buf, " off");
				}
			}
		}
		if (pmono || cmono)
			printf("\n");
		if (!pmono || !cmono) {
			for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++) {
				if ((pmono || !snd_mixer_selem_has_playback_channel(elem, chn)) &&
				    (cmono || !snd_mixer_selem_has_capture_channel(elem, chn)))
					continue;
				printf("%s%s:", space, snd_mixer_selem_channel_name(chn));
				if (!pmono && !cmono && snd_mixer_selem_has_common_volume(elem)) {
					char *str = NULL;
					show_selem_volume(elem, chn, 0, pmin, pmax, &str);
					strcat(*buf, str);
					free(str);
				}
				if (!pmono && !cmono && snd_mixer_selem_has_common_switch(elem)) {
					snd_mixer_selem_get_playback_switch(elem, chn, &psw);
					printf(" [%s]", psw ? "on" : "off");
					psw ? strcat(*buf, " on") : strcat(*buf, " off");
				}
				if (!pmono && snd_mixer_selem_has_playback_channel(elem, chn)) {
					int title = 0;
					if (!snd_mixer_selem_has_common_volume(elem)) {
						if (snd_mixer_selem_has_playback_volume(elem)) {
							char *str = NULL;
							printf(" Playback");
							title = 1;
							show_selem_volume(elem, chn, 0, pmin, pmax, &str);
							strcat(*buf, str);
							free(str);
						}
					}
					if (!snd_mixer_selem_has_common_switch(elem)) {
						if (snd_mixer_selem_has_playback_switch(elem)) {
							if (!title)
								printf(" Playback");
							snd_mixer_selem_get_playback_switch(elem, chn, &psw);
							printf(" [%s]", psw ? "on" : "off");
							psw ? strcat(*buf, " on") : strcat(*buf, " off");
						}
					}
				}
				if (!cmono && snd_mixer_selem_has_capture_channel(elem, chn)) {
					int title = 0;
					if (!snd_mixer_selem_has_common_volume(elem)) {
						if (snd_mixer_selem_has_capture_volume(elem)) {
							char *str = NULL;
							printf(" Capture");
							title = 1;
							show_selem_volume(elem, chn, 1, cmin, cmax, &str);
							strcat(*buf, str);
							free(str);
						}
					}
					if (!snd_mixer_selem_has_common_switch(elem)) {
						if (snd_mixer_selem_has_capture_switch(elem)) {
							if (!title)
								printf(" Capture");
							snd_mixer_selem_get_capture_switch(elem, chn, &csw);
							printf(" [%s]", csw ? "on" : "off");
							csw ? strcat(*buf, " on") : strcat(*buf, " off");
						}
					}
				}
				printf("\n");
				strcat(*buf, "\n");
			}
		}
	}
	
	return 0;
}


static void sevents_value(snd_mixer_selem_id_t *sid)
{
	char *buf = NULL;		
	buf = (char*)malloc(sizeof(char) * MSG_SIZE);
	if (!buf) {
		fprintf(stderr, "Memory allocation failure\n");
		exit (EXIT_FAILURE);
	}	
	sprintf(buf, "'%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
	show_selem(sid, "  ", 1, &buf);
	send_msg(buf);
	free(buf);
}

static void sevents_info(snd_mixer_selem_id_t *sid)
{
	printf("event info: '%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
}

static void sevents_remove(snd_mixer_selem_id_t *sid)
{
	printf("event remove: '%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
}

static int melem_event(snd_mixer_elem_t *elem, unsigned int mask)
{
	snd_mixer_selem_id_t *sid;
	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_get_id(elem, sid);
	if (mask == SND_CTL_EVENT_MASK_REMOVE) {
		sevents_remove(sid);
		return 0;
	}
	if (mask & SND_CTL_EVENT_MASK_INFO) 
		sevents_info(sid);
	if (mask & SND_CTL_EVENT_MASK_VALUE)
		sevents_value(sid);
	
	return 0;
}

static void sevents_add(snd_mixer_elem_t *elem)
{
	snd_mixer_selem_id_t *sid;
	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_get_id(elem, sid);
	printf("event add: '%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
	snd_mixer_elem_set_callback(elem, melem_event);
}

static int mixer_event(snd_mixer_t *mixer, unsigned int mask,
		snd_mixer_elem_t *elem)
{
	if (mask & SND_CTL_EVENT_MASK_ADD)
		sevents_add(elem);
	return 0;
}

int alsa_monitor_init(const char *snd_card)
{
	int err;

	sprintf(card, "%s", snd_card);
	smixer_options.device = card;

	if ((err = snd_mixer_open(&handle, 0)) < 0) {
		error( "Mixer %s open error: %s", card, snd_strerror(err));
		return err;
	}
	if (smixer_level == 0 && (err = snd_mixer_attach(handle, card)) < 0) {
		error( "Mixer attach %s error: %s", card, snd_strerror(err));
		snd_mixer_close(handle);
		return err;
	}
	if ((err = snd_mixer_selem_register(handle, smixer_level > 0 ? &smixer_options : NULL, NULL)) < 0) {
		error( "Mixer register error: %s", snd_strerror(err));
		snd_mixer_close(handle);
		return err;
	}
	snd_mixer_set_callback(handle, mixer_event);
	err = snd_mixer_load(handle);
	if (err < 0) {
		error( "Mixer %s load error: %s", card, snd_strerror(err));
		snd_mixer_close(handle);
		return err;
	}

	printf("Ready to listen...\n");
	while (1) {
		int res;
		res = snd_mixer_wait(handle, -1);
		if (res >= 0) {
			printf("Poll ok: %i\n", res);
			res = snd_mixer_handle_events(handle);
			assert(res >= 0);
		}
	}
	snd_mixer_close(handle);
	snd_config_update_free_global();
	return 0;
}

int send_msg(const char *msg)
{
    int s;
    socklen_t len;
    struct sockaddr_un remote;
    char str[MSG_SIZE] = {0};
    const char *user_home;
    char *sockpath;

    if ((s = socket(AF_UNIX, SOCK_SEQPACKET | SOCK_CLOEXEC, 0)) == -1) {
        perror("socket");
        exit(1);
    }  
  
	if( userinfo_init() ) {
		
		fprintf(stderr, "Cannot retrieve username and home: %s\n", strerror(errno));
    }
  
	user_home = userhome();
		
	sockpath = (char*)malloc(sizeof(char) * 128);
	if (!sockpath) {
		fprintf(stderr, "Memory allocation failure\n");
		exit (EXIT_FAILURE);
	}
	
	strcpy(sockpath, user_home);
	strcat(sockpath, "/");
	strcat(sockpath, SOCK_NAME);

    //printf("Trying to connect...\n");

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, sockpath);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect");
        exit(1);
    }

    //printf("Connected.\n");
#if 0
	socklen_t t;
	
    while(printf("> "), fgets(str, MSG_SIZE, stdin), !feof(stdin)) {
        if (send(s, str, strlen(str), 0) == -1) {
            perror("send");
            exit(1);
        }

        if ((sock=recv(s, str, MSG_SIZE, 0)) > 0) {
            str[sock] = '\0';
            printf("echo> %s", str);
        } else {
            if (sock < 0) perror("recv");
            else printf("Server closed connection\n");
            exit(1);
        }
    }
#else
    if (send(s, msg, strlen(msg), 0) < 0)
	    printf("Send failed\n");
#endif
    close(s);
    
    free(sockpath);

    return 0;
}

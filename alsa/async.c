/*
 * async.c
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>
#include <wait.h>
#include <getopt.h>

#include <sys/socket.h>
#include <sys/types.h>

#define DATA1 ""
#define DATA2 ""

#include <globals.h>

int run_async_snd_monitor(const char *snd_card)
{
	int rc = 0;
	pid_t pid = 0;
	pid_t sid = 0;
	int sockets[2], child;
	char buf[1024];
   
	if (socketpair(AF_UNIX, SOCK_STREAM, 0, sockets) < 0) {
		perror("opening stream socket pair");
		exit(1);
	}
   
   pid = fork();
   
   if( pid == 0 ) {  /* child */

      //pid_t ppid = getppid();
      
      close(sockets[1]);
      
      if (write(sockets[0], DATA2, sizeof(DATA2)) < 0)
         perror("writing stream message");
      
      close(sockets[0]);
      
      // run function
      return alsa_monitor_init(snd_card);

   }
   else if( pid > 0 ) {
      
      rc = 0;
      
      close(sockets[0]);
      if (read(sockets[1], buf, 1024) < 0)
         perror("reading stream message");
         
      //printf("%s\n", buf);
      
      close(sockets[1]);
      
      return rc;
   }
   else {
      
      // error 
      rc = -errno;

      fprintf(stderr, "fork() failed: %s\n", strerror(errno));
      
      return rc;
   }
}

/*
  ALSA command line mixer utility
  Copyright (c) 1999-2000 by Jaroslav Kysela <perex@perex.cz>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

  All modifications to the original source file are:
  Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <assert.h>
#include <alsa/asoundlib.h>
#include <poll.h>
#include <stdint.h>
#include <stdbool.h>

#include <globals.h>

#define LEVEL_BASIC		(1<<0)
#define LEVEL_INACTIVE	(1<<1)
#define LEVEL_ID		(1<<2)

static int smixer_level = 0;
static int debugflag = 0;
static int no_check = 0;
static int ignore_error = 0;
static struct snd_mixer_selem_regopt smixer_options;
static char card[64];

static void error(const char *fmt,...)
{
	va_list va;

	va_start(va, fmt);
	fprintf(stderr, "amixer: ");
	vfprintf(stderr, fmt, va);
	fprintf(stderr, "\n");
	va_end(va);
}

#define check_range(val, min, max) \
	(no_check ? (val) : ((val < min) ? (min) : (val > max) ? (max) : (val)))

/* Fuction to convert from volume to percentage. val = volume */

static int convert_prange(long val, long min, long max)    /* hack */
{
	long range = max - min;
	int tmp;

	if (range == 0)
		return min;
	val -= min;
	tmp = rint((double)val/(double)range * 100);
	return tmp;
}

/* Function to convert from percentage to volume. perc = percentage */
static long convert_prange1(long perc, long min, long max)
{
	long tmp;

	tmp = rint((double)perc * (double)(max - min) * 0.01);
	if (tmp == 0 && perc > 0)
		tmp++;
	return tmp + min;
}

struct volume_ops {
	int (*get_range)(snd_mixer_elem_t *elem, long *min, long *max);
	int (*get)(snd_mixer_elem_t *elem, snd_mixer_selem_channel_id_t c,
		   long *value);
	int (*set)(snd_mixer_elem_t *elem, snd_mixer_selem_channel_id_t c,
		   long value, int dir);
};
	
enum { VOL_RAW, VOL_DB, VOL_MAP };

struct volume_ops_set {
	int (*has_volume)(snd_mixer_elem_t *elem);
	struct volume_ops v[3];
};

static int set_playback_dB(snd_mixer_elem_t *elem,
			   snd_mixer_selem_channel_id_t c, long value, int dir)
{
	return snd_mixer_selem_set_playback_dB(elem, c, value, dir);
}

static int set_capture_dB(snd_mixer_elem_t *elem,
			  snd_mixer_selem_channel_id_t c, long value, int dir)
{
	return snd_mixer_selem_set_capture_dB(elem, c, value, dir);
}

static int set_playback_raw_volume(snd_mixer_elem_t *elem,
				   snd_mixer_selem_channel_id_t c,
				   long value, int dir)
{
	return snd_mixer_selem_set_playback_volume(elem, c, value);
}

static int set_capture_raw_volume(snd_mixer_elem_t *elem,
				  snd_mixer_selem_channel_id_t c,
				  long value, int dir)
{
	return snd_mixer_selem_set_capture_volume(elem, c, value);
}

/* FIXME: normalize to int32 space to be compatible with other types */
#define MAP_VOL_RES	(INT32_MAX / 100)

static int get_mapped_volume_range(snd_mixer_elem_t *elem,
				   long *pmin, long *pmax)
{
	*pmin = 0;
	*pmax = MAP_VOL_RES;
	return 0;
}

static const struct volume_ops_set vol_ops[2] = {
	{
		.has_volume = snd_mixer_selem_has_playback_volume,
		.v = {{ snd_mixer_selem_get_playback_volume_range,
			snd_mixer_selem_get_playback_volume,
			set_playback_raw_volume },
		      { snd_mixer_selem_get_playback_dB_range,
			snd_mixer_selem_get_playback_dB,
			set_playback_dB },
		},
	},
	{
		.has_volume = snd_mixer_selem_has_capture_volume,
		.v = {{ snd_mixer_selem_get_capture_volume_range,
			snd_mixer_selem_get_capture_volume,
			set_capture_raw_volume },
		      { snd_mixer_selem_get_capture_dB_range,
			snd_mixer_selem_get_capture_dB,
			set_capture_dB },
		},
	},
};

static int std_vol_type = VOL_RAW;

static int set_volume_simple(snd_mixer_elem_t *elem,
			     snd_mixer_selem_channel_id_t chn,
			     char **ptr, int dir)
{
	long val, orig, pmin, pmax;
	char *p = *ptr, *s;
	int invalid = 0, percent = 0, err = 0;
	int vol_type;
	double scale = 1.0;
	int correct = 0;

	if (! vol_ops[dir].has_volume(elem))
		invalid = 1;

	if (*p == ':')
		p++;
	if (*p == '\0' || (!isdigit(*p) && *p != '-'))
		goto skip;

	s = p;
	val = strtol(s, &p, 10);
	if (*p == '.') {
		p++;
		strtol(p, &p, 10);
	}
	if (*p == '%') {
		vol_type = std_vol_type;
		percent = 1;
		p++;
	} else if (toupper(p[0]) == 'D' && toupper(p[1]) == 'B') {
		vol_type = VOL_DB;
		p += 2;
		scale = 100;
	} else {
		vol_type = VOL_RAW;
	}

	if (*p && !strchr(",:+-", *p))
		invalid = 1;

	val = (long)(strtod(s, NULL) * scale);
	if (vol_ops[dir].v[vol_type].get_range(elem, &pmin, &pmax) < 0)
		invalid = 1;
	if (percent)
		val = (long)convert_prange1(val, pmin, pmax);
	if (*p == '+' || *p == '-') {
		if (! invalid) {
			if (vol_ops[dir].v[vol_type].get(elem, chn, &orig) < 0)
				invalid = 1;
			if (*p == '+') {
				val = orig + val;
				correct = 1;
			} else {
				val = orig - val;
				correct = -1;
			}
		}
		p++;
	}

	if (*p && !strchr(",:", *p))
		invalid = 1;

	if (! invalid) {
		val = check_range(val, pmin, pmax);
		err = vol_ops[dir].v[vol_type].set(elem, chn, val, correct);
	}
 skip:
	if (*p == ',')
		p++;
	*ptr = p;
	return err ? err : (invalid ? -ENOENT : 0);
}

static int get_bool_simple(char **ptr, char *str, int invert, int orig)
{
	if (**ptr == ':')
		(*ptr)++;
	if (!strncasecmp(*ptr, str, strlen(str))) {
		orig = 1 ^ (invert ? 1 : 0);
		while (**ptr != '\0' && **ptr != ',' && **ptr != ':')
			(*ptr)++;
	}
	if (**ptr == ',' || **ptr == ':')
		(*ptr)++;
	return orig;
}
		
static int simple_skip_word(char **ptr, char *str)
{
	char *xptr = *ptr;
	if (*xptr == ':')
		xptr++;
	if (!strncasecmp(xptr, str, strlen(str))) {
		while (*xptr != '\0' && *xptr != ',' && *xptr != ':')
			xptr++;
		if (*xptr == ',' || *xptr == ':')
			xptr++;
		*ptr = xptr;
		return 1;
	}
	return 0;
}
		
static void show_control_id(snd_ctl_elem_id_t *id)
{
	char *str;

	str = snd_ctl_ascii_elem_id_get(id);
	if (str)
		printf("%s", str);
	free(str);
}

static void print_spaces(unsigned int spaces)
{
	while (spaces-- > 0)
		putc(' ', stdout);
}

static void print_dB(long dB)
{
	if (dB < 0) {
		printf("-%li.%02lidB", -dB / 100, -dB % 100);
	} else {
		printf("%li.%02lidB", dB / 100, dB % 100);
	}
}

/* hack */
static void show_selem_volume(snd_mixer_elem_t *elem, 
			      snd_mixer_selem_channel_id_t chn, int dir,
			      long min, long max)
{
	long raw, val;
	vol_ops[dir].v[VOL_RAW].get(elem, chn, &raw);
	if (std_vol_type == VOL_RAW) {
		val = convert_prange(raw, min, max);
		ui_limits(ctx, min, max);
	}
	else {
		vol_ops[dir].v[std_vol_type].get(elem, chn, &val);
		val = convert_prange(val, 0, MAP_VOL_RES);
	}
	printf(" %li [%li%%]", raw, val);
	ui_raw(ctx, raw);
	ui_val(ctx, val);	
	if (!vol_ops[dir].v[VOL_DB].get(elem, chn, &val)) {
		printf(" [");
		print_dB(val);
		printf("]");
	}
}

static int show_selem(snd_mixer_t *handle, snd_mixer_selem_id_t *id, const char *space, int level)
{
	snd_mixer_selem_channel_id_t chn;
	long pmin = 0, pmax = 0;
	long cmin = 0, cmax = 0;
	int psw, csw;
	int pmono, cmono, mono_ok = 0;
	snd_mixer_elem_t *elem;
	
	elem = snd_mixer_find_selem(handle, id);
	if (!elem) {
		error("Mixer %s simple element not found", card);
		return -ENOENT;
	}

	if (level & LEVEL_BASIC) {
		printf("%sCapabilities:", space);
		bool Volume = false;
		bool pVolume = false;
		bool cVolume = false;
		bool Switch = false;
		bool SwitchJoined = false;
		bool mono = false;
		if (snd_mixer_selem_has_common_volume(elem)) {
			printf(" volume");
			Volume = true;
			if (snd_mixer_selem_has_playback_volume_joined(elem))
				printf(" volume-joined");
		} else {
			if (snd_mixer_selem_has_playback_volume(elem)) {
				printf(" pvolume");
				pVolume = true;
				if (snd_mixer_selem_has_playback_volume_joined(elem)) 
					printf(" pvolume-joined");
			}
			if (snd_mixer_selem_has_capture_volume(elem)) {
				printf(" cvolume");
				cVolume = true;
				if (snd_mixer_selem_has_capture_volume_joined(elem))
					printf(" cvolume-joined");
			}
		}
		if (snd_mixer_selem_has_common_switch(elem)) {
			printf(" switch");
			Switch = true;
			if (snd_mixer_selem_has_playback_switch_joined(elem)) {
				printf(" switch-joined");
				SwitchJoined = true;
			}
		} else {
			if (snd_mixer_selem_has_playback_switch(elem)) {
				printf(" pswitch");
				Switch = true;
				if (snd_mixer_selem_has_playback_switch_joined(elem)) {
					printf(" pswitch-joined", strdup(snd_mixer_selem_id_get_name(id)));
					SwitchJoined = true;
				}
			}
			if (snd_mixer_selem_has_capture_switch(elem)) {
				printf(" cswitch");
				Switch = true;
				if (snd_mixer_selem_has_capture_switch_joined(elem)) {
					printf(" cswitch-joined");
					SwitchJoined = true;
				}
				if (snd_mixer_selem_has_capture_switch_exclusive(elem))
					printf(" cswitch-exclusive");
			}
		}
		if (snd_mixer_selem_is_enum_playback(elem)) {
			printf(" penum");
		} else if (snd_mixer_selem_is_enum_capture(elem)) {
			printf(" cenum");
		} else if (snd_mixer_selem_is_enumerated(elem)) {
			printf(" enum");
		}
		printf("\n");
		ui_volume(ctx, Volume, pVolume, cVolume);
		ui_switch(ctx, Switch, SwitchJoined);
		if (snd_mixer_selem_is_enumerated(elem)) {
			int i, items;
			unsigned int idx;
			/*
			 * See snd_ctl_elem_init_enum_names() in
			 * sound/core/control.c.
			 */
			char itemname[64];
			items = snd_mixer_selem_get_enum_items(elem);
			printf("  Items:");
			for (i = 0; i < items; i++) {
				snd_mixer_selem_get_enum_item_name(elem, i, sizeof(itemname) - 1, itemname);
				printf(" '%s'", itemname);
			}
			printf("\n");
			for (i = 0; !snd_mixer_selem_get_enum_item(elem, i, &idx); i++) {
				snd_mixer_selem_get_enum_item_name(elem, idx, sizeof(itemname) - 1, itemname);
				printf("  Item%d: '%s'\n", i, itemname);
			}
			return 0; /* no more thing to do */
		}
		if (snd_mixer_selem_has_capture_switch_exclusive(elem))
			printf("%sCapture exclusive group: %i\n", space,
			       snd_mixer_selem_get_capture_group(elem));
		if (snd_mixer_selem_has_playback_volume(elem) ||
		    snd_mixer_selem_has_playback_switch(elem)) {
			printf("%sPlayback channels:", space);
			if (snd_mixer_selem_is_playback_mono(elem)) {
				printf(" Mono");
				ui_mono(ctx, true);
			} else {
				int first = 1;
				for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++){
					if (!snd_mixer_selem_has_playback_channel(elem, chn))
						continue;
					if (!first)
						printf(" -");
					printf(" %s", snd_mixer_selem_channel_name(chn));
					first = 0;
				}
			}
			printf("\n");
		}
		if (snd_mixer_selem_has_capture_volume(elem) ||
		    snd_mixer_selem_has_capture_switch(elem)) {
			printf("%sCapture channels:", space);
			if (snd_mixer_selem_is_capture_mono(elem)) {
				printf(" Mono");
				ui_mono(ctx, true);
			} else {
				int first = 1;
				for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++){
					if (!snd_mixer_selem_has_capture_channel(elem, chn))
						continue;
					if (!first)
						printf(" -");
					printf(" %s", snd_mixer_selem_channel_name(chn));
					first = 0;
				}
			}
			printf("\n");
		}
		if (snd_mixer_selem_has_playback_volume(elem) ||
		    snd_mixer_selem_has_capture_volume(elem)) {
			printf("%sLimits:", space);
			if (snd_mixer_selem_has_common_volume(elem)) {
				snd_mixer_selem_get_playback_volume_range(elem, &pmin, &pmax);
				snd_mixer_selem_get_capture_volume_range(elem, &cmin, &cmax);
				printf(" %li - %li", pmin, pmax);
			} else {
				if (snd_mixer_selem_has_playback_volume(elem)) {
					snd_mixer_selem_get_playback_volume_range(elem, &pmin, &pmax);
					printf(" Playback %li - %li", pmin, pmax);
				}
				if (snd_mixer_selem_has_capture_volume(elem)) {
					snd_mixer_selem_get_capture_volume_range(elem, &cmin, &cmax);
					printf(" Capture %li - %li", cmin, cmax);
				}
			}
			printf("\n");
		}
		pmono = snd_mixer_selem_has_playback_channel(elem, SND_MIXER_SCHN_MONO) &&
		        (snd_mixer_selem_is_playback_mono(elem) || 
			 (!snd_mixer_selem_has_playback_volume(elem) &&
			  !snd_mixer_selem_has_playback_switch(elem)));
		cmono = snd_mixer_selem_has_capture_channel(elem, SND_MIXER_SCHN_MONO) &&
		        (snd_mixer_selem_is_capture_mono(elem) || 
			 (!snd_mixer_selem_has_capture_volume(elem) &&
			  !snd_mixer_selem_has_capture_switch(elem)));
#if 0
		printf("pmono = %i, cmono = %i (%i, %i, %i, %i)\n", pmono, cmono,
				snd_mixer_selem_has_capture_channel(elem, SND_MIXER_SCHN_MONO),
				snd_mixer_selem_is_capture_mono(elem),
				snd_mixer_selem_has_capture_volume(elem),
				snd_mixer_selem_has_capture_switch(elem));
#endif
		if (pmono || cmono) {
			if (!mono_ok) {
				printf("%s%s:", space, "Mono");
				mono_ok = 1;
			}
			if (snd_mixer_selem_has_common_volume(elem)) {
				show_selem_volume(elem, SND_MIXER_SCHN_MONO, 0, pmin, pmax);
			}
			if (snd_mixer_selem_has_common_switch(elem)) {
				snd_mixer_selem_get_playback_switch(elem, SND_MIXER_SCHN_MONO, &psw);
				printf(" [%s]", psw ? "on" : "off");
				psw ? ui_muted(ctx, true) : ui_muted(ctx, false);
			}
		}
		if (pmono && snd_mixer_selem_has_playback_channel(elem, SND_MIXER_SCHN_MONO)) {
			int title = 0;
			if (!mono_ok) {
				printf("%s%s:", space, "Mono");
				mono_ok = 1;
			}
			if (!snd_mixer_selem_has_common_volume(elem)) {
				if (snd_mixer_selem_has_playback_volume(elem)) {
					printf(" Playback");
					title = 1;
					show_selem_volume(elem, SND_MIXER_SCHN_MONO, 0, pmin, pmax);
				}
			}
			if (!snd_mixer_selem_has_common_switch(elem)) {
				if (snd_mixer_selem_has_playback_switch(elem)) {
					if (!title)
						printf(" Playback");
					snd_mixer_selem_get_playback_switch(elem, SND_MIXER_SCHN_MONO, &psw);
					printf(" [%s]", psw ? "on" : "off");
					psw ? ui_muted(ctx, true) : ui_muted(ctx, false);
				}
			}
		}
		if (cmono && snd_mixer_selem_has_capture_channel(elem, SND_MIXER_SCHN_MONO)) {
			int title = 0;
			if (!mono_ok) {
				printf("%s%s:", space, "Mono");
				mono_ok = 1;
			}
			if (!snd_mixer_selem_has_common_volume(elem)) {
				if (snd_mixer_selem_has_capture_volume(elem)) {
					printf(" Capture");
					title = 1;
					show_selem_volume(elem, SND_MIXER_SCHN_MONO, 1, cmin, cmax);
				}
			}
			if (!snd_mixer_selem_has_common_switch(elem)) {
				if (snd_mixer_selem_has_capture_switch(elem)) {
					if (!title)
						printf(" Capture");
					snd_mixer_selem_get_capture_switch(elem, SND_MIXER_SCHN_MONO, &csw);
					printf(" [%s]", csw ? "on" : "off");
					csw ? ui_muted(ctx, true) : ui_muted(ctx, false);
				}
			}
		}			
		if (pmono || cmono)
			printf("\n");
		if (!pmono || !cmono) {
			for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++) {
				if ((pmono || !snd_mixer_selem_has_playback_channel(elem, chn)) &&
				    (cmono || !snd_mixer_selem_has_capture_channel(elem, chn)))
					continue;
				printf("%s%s:", space, snd_mixer_selem_channel_name(chn));
				if (!pmono && !cmono && snd_mixer_selem_has_common_volume(elem)) {
					show_selem_volume(elem, chn, 0, pmin, pmax);
				}
				if (!pmono && !cmono && snd_mixer_selem_has_common_switch(elem)) {
					snd_mixer_selem_get_playback_switch(elem, chn, &psw);
					printf(" [%s]", psw ? "on" : "off");
					psw ? ui_muted(ctx, true) : ui_muted(ctx, false);
				}
				if (!pmono && snd_mixer_selem_has_playback_channel(elem, chn)) {
					int title = 0;
					if (!snd_mixer_selem_has_common_volume(elem)) {
						if (snd_mixer_selem_has_playback_volume(elem)) {
							printf(" Playback");
							title = 1;
							show_selem_volume(elem, chn, 0, pmin, pmax);
						}
					}
					if (!snd_mixer_selem_has_common_switch(elem)) {
						if (snd_mixer_selem_has_playback_switch(elem)) {
							if (!title)
								printf(" Playback");
							snd_mixer_selem_get_playback_switch(elem, chn, &psw);
							printf(" [%s]", psw ? "on" : "off");
							psw ? ui_muted(ctx, true) : ui_muted(ctx, false);
						}
					}
				}
				if (!cmono && snd_mixer_selem_has_capture_channel(elem, chn)) {
					int title = 0;
					if (!snd_mixer_selem_has_common_volume(elem)) {
						if (snd_mixer_selem_has_capture_volume(elem)) {
							printf(" Capture");
							title = 1;
							show_selem_volume(elem, chn, 1, cmin, cmax);
						}
					}
					if (!snd_mixer_selem_has_common_switch(elem)) {
						if (snd_mixer_selem_has_capture_switch(elem)) {
							if (!title)
								printf(" Capture");
							snd_mixer_selem_get_capture_switch(elem, chn, &csw);
							printf(" [%s]", csw ? "on" : "off");
							csw ? ui_muted(ctx, true) : ui_muted(ctx, false);
						}
					}
				}
				printf("\n");
			}
		}
	}
	return 0;
}

int selems(int level)
{
	int err;
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;
	snd_mixer_elem_t *elem;
	snd_mixer_selem_id_alloca(&sid);
	
	if ((err = snd_mixer_open(&handle, 0)) < 0) {
		error("Mixer %s open error: %s", card, snd_strerror(err));
		return err;
	}
	if (smixer_level == 0 && (err = snd_mixer_attach(handle, card)) < 0) {
		error("Mixer attach %s error: %s", card, snd_strerror(err));
		snd_mixer_close(handle);
		return err;
	}
	if ((err = snd_mixer_selem_register(handle, smixer_level > 0 ? &smixer_options : NULL, NULL)) < 0) {
		error("Mixer register error: %s", snd_strerror(err));
		snd_mixer_close(handle);
		return err;
	}
	err = snd_mixer_load(handle);
	if (err < 0) {
		error("Mixer %s load error: %s", card, snd_strerror(err));
		snd_mixer_close(handle);
		return err;
	}
	for (elem = snd_mixer_first_elem(handle); elem; elem = snd_mixer_elem_next(elem)) {
		snd_mixer_selem_get_id(elem, sid);
		if (!(level & LEVEL_INACTIVE) && !snd_mixer_selem_is_active(elem))
			continue;
		printf("Simple mixer control '%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
		show_selem(handle, sid, "  ", level);
		if(!level) {
			append(strdup(snd_mixer_selem_id_get_name(sid)), snd_mixer_selem_id_get_index(sid), clist);
		}
	}
	snd_mixer_close(handle);
	
	return 0;
}

static int parse_simple_id(const char *str, snd_mixer_selem_id_t *sid)
{
	int c, size;
	char buf[128];
	char *ptr = buf;

	while (*str == ' ' || *str == '\t')
		str++;
	if (!(*str))
		return -EINVAL;
	size = 1;	/* for '\0' */
	if (*str != '"' && *str != '\'') {
		while (*str && *str != ',') {
			if (size < (int)sizeof(buf)) {
				*ptr++ = *str;
				size++;
			}
			str++;
		}
	} else {
		c = *str++;
		while (*str && *str != c) {
			if (size < (int)sizeof(buf)) {
				*ptr++ = *str;
				size++;
			}
			str++;
		}
		if (*str == c)
			str++;
	}
	if (*str == '\0') {
		snd_mixer_selem_id_set_index(sid, 0);
		*ptr = 0;
		goto _set;
	}
	if (*str != ',')
		return -EINVAL;
	*ptr = 0;	/* terminate the string */
	str++;
	if (!isdigit(*str))
		return -EINVAL;
	snd_mixer_selem_id_set_index(sid, atoi(str));
       _set:
	snd_mixer_selem_id_set_name(sid, buf);
	return 0;
}

typedef struct channel_mask {
	char *name;
	unsigned int mask;
} channel_mask_t;
static const channel_mask_t chanmask[] = {
	{"frontleft", 1 << SND_MIXER_SCHN_FRONT_LEFT},
	{"frontright", 1 << SND_MIXER_SCHN_FRONT_RIGHT},
	{"frontcenter", 1 << SND_MIXER_SCHN_FRONT_CENTER},
	{"front", ((1 << SND_MIXER_SCHN_FRONT_LEFT) |
		   (1 << SND_MIXER_SCHN_FRONT_RIGHT))},
	{"center", 1 << SND_MIXER_SCHN_FRONT_CENTER},
	{"rearleft", 1 << SND_MIXER_SCHN_REAR_LEFT},
	{"rearright", 1 << SND_MIXER_SCHN_REAR_RIGHT},
	{"rear", ((1 << SND_MIXER_SCHN_REAR_LEFT) |
		  (1 << SND_MIXER_SCHN_REAR_RIGHT))},
	{"woofer", 1 << SND_MIXER_SCHN_WOOFER},
	{NULL, 0}
};

static unsigned int channels_mask(char **arg, unsigned int def)
{
	const channel_mask_t *c;

	for (c = chanmask; c->name; c++) {
		if (strncasecmp(*arg, c->name, strlen(c->name)) == 0) {
			while (**arg != '\0' && **arg != ',' && **arg != ' ' && **arg != '\t')
				(*arg)++;
			if (**arg == ',' || **arg == ' ' || **arg == '\t')
				(*arg)++;
			return c->mask;
		}
	}
	return def;
}

static unsigned int dir_mask(char **arg, unsigned int def)
{
	int findend = 0;

	if (strncasecmp(*arg, "playback", 8) == 0)
		def = findend = 1;
	else if (strncasecmp(*arg, "capture", 8) == 0)
		def = findend = 2;
	if (findend) {
		while (**arg != '\0' && **arg != ',' && **arg != ' ' && **arg != '\t')
			(*arg)++;
		if (**arg == ',' || **arg == ' ' || **arg == '\t')
			(*arg)++;
	}
	return def;
}

static int get_enum_item_index(snd_mixer_elem_t *elem, char **ptrp)
{
	char *ptr = *ptrp;
	int items, i, len;

	/* See snd_ctl_elem_init_enum_names() in sound/core/control.c. */
	char name[64];
	
	items = snd_mixer_selem_get_enum_items(elem);
	if (items <= 0)
		return -1;

	for (i = 0; i < items; i++) {
		if (snd_mixer_selem_get_enum_item_name(elem, i, sizeof(name)-1, name) < 0)
			continue;

		len = strlen(name);
		if (! strncmp(name, ptr, len)) {
			if (! ptr[len] || ptr[len] == ',' || ptr[len] == '\n') {
				ptr += len;
				*ptrp = ptr;
				return i;
			}
		}
	}
	return -1;
}

static int sset_enum(snd_mixer_elem_t *elem, unsigned int argc, char **argv)
{
	unsigned int idx, item = 0;
	int check_flag = ignore_error ? 0 : -1;

	for (idx = 1; idx < argc; idx++) {
		char *ptr = argv[idx];
		while (*ptr) {
			int ival = get_enum_item_index(elem, &ptr);
			if (ival < 0)
				return check_flag;
			if (snd_mixer_selem_set_enum_item(elem, item++, ival) >= 0)
				check_flag = 1;
			/* skip separators */
			while (*ptr == ',' || isspace(*ptr))
				ptr++;
		}
	}
	return check_flag;
}

static int sset_channels(snd_mixer_elem_t *elem, unsigned int argc, char **argv)
{
	unsigned int channels = ~0U;
	unsigned int dir = 3, okflag = 3;
	unsigned int idx;
	snd_mixer_selem_channel_id_t chn;
	int check_flag = ignore_error ? 0 : -1;

	for (idx = 1; idx < argc-1; idx++) {
		char *ptr = argv[idx], *optr;
		int multi, firstchn = 1;
		channels = channels_mask(&ptr, channels);
		if (*ptr == '\0')
			continue;
		dir = dir_mask(&ptr, dir);
		if (*ptr == '\0')
			continue;
		multi = (strchr(ptr, ',') != NULL);
		optr = ptr;
		/* hack */
		// for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++) {
		chn = atoi(argv[argc-1]);
			char *sptr = NULL;
			int ival;

			//if (!(channels & (1 << chn)))
				//continue;

			if ((dir & 1) && snd_mixer_selem_has_playback_channel(elem, chn)) {
				sptr = ptr;
				if (!strncmp(ptr, "mute", 4) && snd_mixer_selem_has_playback_switch(elem)) {
					snd_mixer_selem_get_playback_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_playback_switch(elem, chn, get_bool_simple(&ptr, "mute", 1, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "off", 3) && snd_mixer_selem_has_playback_switch(elem)) {
					snd_mixer_selem_get_playback_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_playback_switch(elem, chn, get_bool_simple(&ptr, "off", 1, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "unmute", 6) && snd_mixer_selem_has_playback_switch(elem)) {
					snd_mixer_selem_get_playback_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_playback_switch(elem, chn, get_bool_simple(&ptr, "unmute", 0, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "on", 2) && snd_mixer_selem_has_playback_switch(elem)) {
					snd_mixer_selem_get_playback_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_playback_switch(elem, chn, get_bool_simple(&ptr, "on", 0, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "toggle", 6) && snd_mixer_selem_has_playback_switch(elem)) {
					if (firstchn || !snd_mixer_selem_has_playback_switch_joined(elem)) {
						snd_mixer_selem_get_playback_switch(elem, chn, &ival);
						if (snd_mixer_selem_set_playback_switch(elem, chn, (ival ? 1 : 0) ^ 1) >= 0)
							check_flag = 1;
					}
					simple_skip_word(&ptr, "toggle");
				} else if (isdigit(*ptr) || *ptr == '-' || *ptr == '+') {
					if (set_volume_simple(elem, chn, &ptr, 0) >= 0)
						check_flag = 1;
				} else if (simple_skip_word(&ptr, "cap") || simple_skip_word(&ptr, "rec") ||
					   simple_skip_word(&ptr, "nocap") || simple_skip_word(&ptr, "norec")) {
					/* nothing */
				} else {
					okflag &= ~1;
				}
			}
			if ((dir & 2) && snd_mixer_selem_has_capture_channel(elem, chn)) {
				if (sptr != NULL)
					ptr = sptr;
				sptr = ptr;
				if (!strncmp(ptr, "cap", 3) && snd_mixer_selem_has_capture_switch(elem)) {
					snd_mixer_selem_get_capture_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_capture_switch(elem, chn, get_bool_simple(&ptr, "cap", 0, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "rec", 3) && snd_mixer_selem_has_capture_switch(elem)) {
					snd_mixer_selem_get_capture_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_capture_switch(elem, chn, get_bool_simple(&ptr, "rec", 0, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "nocap", 5) && snd_mixer_selem_has_capture_switch(elem)) {
					snd_mixer_selem_get_capture_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_capture_switch(elem, chn, get_bool_simple(&ptr, "nocap", 1, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "norec", 5) && snd_mixer_selem_has_capture_switch(elem)) {
					snd_mixer_selem_get_capture_switch(elem, chn, &ival);
					if (snd_mixer_selem_set_capture_switch(elem, chn, get_bool_simple(&ptr, "norec", 1, ival)) >= 0)
						check_flag = 1;
				} else if (!strncmp(ptr, "toggle", 6) && snd_mixer_selem_has_capture_switch(elem)) {
					if (firstchn || !snd_mixer_selem_has_capture_switch_joined(elem)) {
						snd_mixer_selem_get_capture_switch(elem, chn, &ival);
						if (snd_mixer_selem_set_capture_switch(elem, chn, (ival ? 1 : 0) ^ 1) >= 0)
							check_flag = 1;
					}
					simple_skip_word(&ptr, "toggle");
				} else if (isdigit(*ptr) || *ptr == '-' || *ptr == '+') {
					if (set_volume_simple(elem, chn, &ptr, 1) >= 0)
						check_flag = 1;
				} else if (simple_skip_word(&ptr, "mute") || simple_skip_word(&ptr, "off") ||
					   simple_skip_word(&ptr, "unmute") || simple_skip_word(&ptr, "on")) {
					/* nothing */
				} else {
					okflag &= ~2;
				}
			}
			if (okflag == 0) {
				if (debugflag) {
					if (dir & 1)
						error("Unknown playback setup '%s'..", ptr);
					if (dir & 2)
						error("Unknown capture setup '%s'..", ptr);
				}
				return 0; /* just skip it */
			}
			if (!multi)
				ptr = optr;
			firstchn = 0;
		//} /* hack */
	}
	return check_flag;
}

static int sset(unsigned int n, const char *params[], int roflag)
{
	int err = 0;
	static snd_mixer_t *handle = NULL;
	snd_mixer_elem_t *elem;
	snd_mixer_selem_id_t *sid;
	snd_mixer_selem_id_alloca(&sid);
	
	if (n < 1) {
		fprintf(stderr, "Specify a scontrol identifier: 'name',index\n");
		return 1;
	}
	if (parse_simple_id(params[0], sid)) {
		fprintf(stderr, "Wrong scontrol identifier: %s\n", params[0]);
		return 1;
	}
	if (handle == NULL) {
		if ((err = snd_mixer_open(&handle, 0)) < 0) {
			error("Mixer %s open error: %s\n", card, snd_strerror(err));
			return err;
		}
		if (smixer_level == 0 && (err = snd_mixer_attach(handle, card)) < 0) {
			error("Mixer attach %s error: %s", card, snd_strerror(err));
			snd_mixer_close(handle);
			handle = NULL;
			return err;
		}
		if ((err = snd_mixer_selem_register(handle, smixer_level > 0 ? &smixer_options : NULL, NULL)) < 0) {
			error("Mixer register error: %s", snd_strerror(err));
			snd_mixer_close(handle);
			handle = NULL;
			return err;
		}
		err = snd_mixer_load(handle);
		if (err < 0) {
			error("Mixer %s load error: %s", card, snd_strerror(err));
			snd_mixer_close(handle);
			handle = NULL;
			return err;
		}
	}
	elem = snd_mixer_find_selem(handle, sid);
	if (!elem) {
		if (ignore_error)
			return 0;
		error("Unable to find simple control '%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
		snd_mixer_close(handle);
		handle = NULL;
		return -ENOENT;
	}
	if (!roflag) {
		if(n < 2) {
			fprintf(stderr, "Specify what you want to set...\n");
			err = 1;
			goto done;
		}
		/* enum control */
		if (snd_mixer_selem_is_enumerated(elem))
			err = sset_enum(elem, n, (char**)params);
		else 
			err = sset_channels(elem, n, (char**)params);

		if (!err)
			goto done;
		if (err < 0) {
			error("Invalid command!");
			goto done;
		}
	}
	
	printf("Simple mixer control '%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
	ui_selem_name(ctx, strdup(snd_mixer_selem_id_get_name(sid)));
	ui_index(ctx, snd_mixer_selem_id_get_index(sid));
	show_selem(handle, sid, "  ", 1);
		
done:
	snd_mixer_close(handle);
	handle = NULL;

	return err < 0 ? 1 : 0;
}

/*
 * split a line into tokens
 * the content in the line buffer is modified
 */
static int split_line(char *buf, char **token, int max_token)
{
	char *dst;
	int n, esc, quote;

	for (n = 0; n < max_token; n++) {
		while (isspace(*buf))
			buf++;
		if (! *buf || *buf == '\n')
			return n;
		/* skip comments */
		if (*buf == '#' || *buf == '!')
			return n;
		esc = 0;
		quote = 0;
		token[n] = buf;
		for (dst = buf; *buf && *buf != '\n'; buf++) {
			if (esc)
				esc = 0;
			else if (isspace(*buf) && !quote) {
				buf++;
				break;
			} else if (*buf == '\\') {
				esc = 1;
				continue;
			} else if (*buf == '\'' || *buf == '"') {
				if (! quote) {
					quote = *buf;
					continue;
				} else if (*buf == quote) {
					quote = 0;
					continue;
				}
			}
			*dst++ = *buf;
		}
		*dst = 0;
	}
	return n;
}

static void get_selem_volume(snd_mixer_elem_t *elem, 
			      snd_mixer_selem_channel_id_t chn, int dir,
			      long min, long max, char **buf)
{
	long raw, val;
	
	vol_ops[dir].v[VOL_RAW].get(elem, chn, &raw);
	if (std_vol_type == VOL_RAW)
		val = convert_prange(raw, min, max);
	else {
		vol_ops[dir].v[std_vol_type].get(elem, chn, &val);
		val = convert_prange(val, 0, MAP_VOL_RES);
	}
	if (!vol_ops[dir].v[VOL_DB].get(elem, chn, &val)) {	
		if (val < 0) {
			sprintf(*buf, "%li -%li.%02li", raw, -val / 100, -val % 100);
		} else {
			sprintf(*buf, "%li %li.%02li", raw, val / 100, val % 100);
		}
	}
}

int update_values( unsigned int argc, char *argv[], const char *snd_card, snd_mixer_selem_channel_id_t chn_id, int dir, long pmin, long pmax, char **buf )
{
	int err = 0;
	static snd_mixer_t *handle = NULL;
	snd_mixer_elem_t *elem;
	snd_mixer_selem_id_t *sid;
	snd_mixer_selem_id_alloca(&sid);

	sprintf(card, "%s", snd_card);
	smixer_options.device = card;
		
	*buf = (char*)malloc(sizeof(char) * 64);
	if (!*buf) {
		fprintf(stderr, "Memory allocation failure\n");
		exit (EXIT_FAILURE);
	}

	if (argc < 1) {
		fprintf(stderr, "Specify a scontrol identifier: 'name',index\n");
		return 1;
	}

	if (parse_simple_id(argv[0], sid)) {
		fprintf(stderr, "Wrong scontrol identifier: %s\n", argv[0]);
		return 1;
	}

	if (handle == NULL) {
		if ((err = snd_mixer_open(&handle, 0)) < 0) {
			error("Mixer %s open error: %s\n", card, snd_strerror(err));
			return err;
		}

		if (smixer_level == 0 && (err = snd_mixer_attach(handle, card)) < 0) {
			error("Mixer attach %s error: %s", card, snd_strerror(err));
			snd_mixer_close(handle);
			handle = NULL;
			return err;
		}

		if ((err = snd_mixer_selem_register(handle, smixer_level > 0 ? &smixer_options : NULL, NULL)) < 0) {
			error("Mixer register error: %s", snd_strerror(err));
			snd_mixer_close(handle);
			handle = NULL;
			return err;
		}

		err = snd_mixer_load(handle);
		if (err < 0) {
			error("Mixer %s load error: %s", card, snd_strerror(err));
			snd_mixer_close(handle);
			handle = NULL;
			return err;
		}
	}

	elem = snd_mixer_find_selem(handle, sid);
	if (!elem) {
		if (ignore_error)
			return 0;
		error("Unable to find simple control '%s',%i\n", snd_mixer_selem_id_get_name(sid), snd_mixer_selem_id_get_index(sid));
		snd_mixer_close(handle);
		handle = NULL;
		return -ENOENT;
	}
	
	get_selem_volume(elem, chn_id, dir, pmin, pmax, buf);

done:
	snd_mixer_close(handle);
	handle = NULL;

	return err < 0 ? 1 : 0;
}

int amixer(const char *snd_card, unsigned int n, const char *params[])
{
	int retval, level = 0;

	sprintf(card, "%s", snd_card);
	smixer_options.device = card;
	
	if (n <= 0) {
		retval = selems(LEVEL_BASIC | level) ? 1 : 0;
		goto finish;
	}
	if (!strcmp(params[0], "scontrols")) {
		return selems(level) ? 1 : 0;
	} else if (!strcmp(params[0], "scontents")) {
		return selems(LEVEL_BASIC | level) ? 1 : 0;
	} else if (!strcmp(params[0], "sset")) {
		return sset(n - 1, params + 1, 0) ? 1 : 0;
	} else if (!strcmp(params[0], "sget")) {
		return sset(n - 1, params + 1, 1) ? 1 : 0;
	} else {
		fprintf(stderr, "amixer: Unknown command '%s'...\n", params[0]);
	}

finish:
	snd_config_update_free_global();

	return retval;
}


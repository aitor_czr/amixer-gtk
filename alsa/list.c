/*
 * list.c
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <globals.h>

node_t * createnode(const char * str, int idx)
{
  node_t * item = (node_t *) malloc( sizeof(node_t) );
  if (!item) {
    return NULL;
  }
  item->name = str;
  item->index = idx;
  item->next = NULL;
  return item;
}

scontrols_t * makelist()
{
  scontrols_t * clist = (scontrols_t *) malloc( sizeof(scontrols_t) );
  if (!clist) {
    return NULL;
  }
  clist->head = NULL;
  return clist;
}

void display(scontrols_t * clist)
{
  node_t * curr = clist->head;
  if(clist->head == NULL) 
    return;
  
  for(; curr != NULL; curr = curr->next) {
    printf("'%s',%d\n", curr->name, curr->index);
  }
}

bool find_control(scontrols_t * clist, const char *name)
{
  bool found_ok = false;
      
  node_t * curr = clist->head;
  if(clist->head == NULL) 
     return false;
  
  for(; curr != NULL; curr = curr->next) {
     char str[32];
     snprintf(str, sizeof(str), "'%s',%d", curr->name, curr->index);
	 if(!strcmp(str, name)) {
	    found_ok = true;
	    break;
	 }
  }  
  
  return found_ok;
}

void append(const char * str, int idx, scontrols_t * clist)
{
  node_t * curr = NULL;
  if(clist->head == NULL){
    clist->head = createnode(str, idx);
  }
  else {
    curr = clist->head; 
    while (curr->next!=NULL){
      curr = curr->next;
    }
    curr->next = createnode(str, idx);
  }
}

void clear(scontrols_t * clist)
{
  node_t * curr = clist->head;
  node_t * next = curr;
  while(curr != NULL){
    next = curr->next;
    free(curr);
    curr = next;
  }
}

void destroy(scontrols_t * clist)
{
  node_t * curr = clist->head;
  node_t * next = curr;
  while(curr != NULL){
    next = curr->next;
    free(curr);
    curr = next;
  }
  free(clist);
}
